import { NgModule } from '@angular/core';
import { TimeAgoPipe } from './time-ago/time-ago';
import { SanitizerPipe } from './sanitizer/sanitizer';
@NgModule({
    declarations: [TimeAgoPipe,
        SanitizerPipe],
    imports: [],
    exports: [TimeAgoPipe,
        SanitizerPipe]
})
export class PipesModule { }
