import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { HttpClientModule } from '@angular/common/http';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClient } from '@angular/common/http';
import { AdMobFree } from '@ionic-native/admob-free';
import { IonicImageViewerModule } from 'ionic-img-viewer';
//import { OneSignal } from '@ionic-native/onesignal';

//UPLOAD PICTURE
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

//SERVICES
import { AuthService } from './service/auth.service';
import { CategorysService } from './service/categorys.service';
import { ProductsService } from './service/products.service';
import { UsersService } from './service/users.service';
import { UsersTypesService } from './service/users-types.service';
import { EventsService } from './service/events.service';
import { OrdersService } from './service/orders.service';
import { MessagesService } from './service/messages.service';
import { TipsService } from './service/tips.service';
import { AccessService } from './service/access.service';
import { ModulesService } from './service/module.service';
import { NewsService } from './service/news.service';
import { UserEventsService } from './service/userevents.service';
import { AdoptionDogsService } from './service/adoption-dogs.service';
import { AdoptionCatsService } from './service/adoption-cats.service';
import { BlogService } from './service/blog.service';
import { WorkoutsService } from './service/workouts.service';
import { MotivationService } from './service/motivation.service';
import { ChallengeService } from './service/challenge.service';
import { ActivePauseService } from './service/activepause.service';
import { SocialService } from './service/social.service';

//PAGES ADMINISTRATOR
import { AdministratorPage } from '../pages/administrator/administrator';
import { ProductsAdminPage } from '../pages/administrator/products-admin/products-admin';
import { ProductsFormAdminPage } from '../pages/administrator/products-admin/form-products-admin/form-products-admin';
import { ProductsFormUpdateAdminPage } from '../pages/administrator/products-admin/form-products-update-admin/form-products-update-admin';
import { SeeProductsAdminPage } from '../pages/administrator/products-admin/see-products-admin/see-products-admin';
import { EventsAdminPage } from '../pages/administrator/events-admin/events-admin';
import { EventsFormAdminPage } from '../pages/administrator/events-admin/form-events-admin/form-events-admin';
import { EventsFormUpdateAdminPage } from '../pages/administrator/events-admin/form-events-update-admin/form-events-update-admin';
import { SeeEventsAdminPage } from '../pages/administrator/events-admin/see-events-admin/see-events-admin';
import { ProfileAdminPage } from '../pages/administrator/profile-admin/profile-admin';
import { ProfileUpdateAdminPage } from '../pages/administrator/profile-admin/profile-update-admin/profile-update-admin';
import { ChangePasswordProfileAdminPage } from '../pages/administrator/profile-admin/change-password-admin/change-password-admin';

//PAGES CLIENTE
import { RegisterPage } from '../pages/register/register';
import { ClientPage } from '../pages/client/client';
import { CategorysClientPage } from '../pages/client/categorys-client/categorys-client';
import { ProductsClientPage } from '../pages/client/products-client/products-client';
import { OrdersClientPage } from '../pages/client/orders-client/orders-client';
import { MyOrdersClientPage } from '../pages/client/orders-client/my-orders/my-orders';
import { ProfileClientPage } from '../pages/client/profile-client/profile-client';
import { ChangePasswordProfileClientPage } from '../pages/client/profile-client/change-password-client/change-password-client';
import { ProfileUpdateClientPage } from '../pages/client/profile-client/profile-update-client/profile-update-client';
import { MessagesClientPage } from '../pages/client/messages-client/messages-client';
import { SeeEventsClientPage } from '../pages/client/events-client/see-events-client/see-events-client';
import { SeeEventsClientCommentPage } from '../pages/client/events-client/see-events-client/see-events-client-comment';
import { SeeProductsClientPage } from '../pages/client/products-client/see-products-client/see-products-client';
import { SeeProductsClientCommentPage } from '../pages/client/products-client/see-products-client/see-products-client-comment';
import { EventsClientPage } from '../pages/client/events-client/events-client';
import { PersonalizationClientPage } from '../pages/client/personalization-client/personalization-client';
import { TipsClientPage } from '../pages/client/tips-client/tips-client';
import { FormTipsClientPage } from '../pages/client/tips-client/form-tips-client/form-tips-client';
import { LeadearboardClientPage } from '../pages/client/leadearboard-client/leadearboard-client';

//PAGES USUARIO
import { UserPage } from '../pages/user/user';
import { CategorysUserPage } from '../pages/user/categorys-user/categorys-user';
import { ProductsUserPage } from '../pages/user/products-user/products-user';
import { ProfileUserPage } from '../pages/user/profile-user/profile-user';
import { ChangePasswordProfileUserPage } from '../pages/user/profile-user/change-password-user/change-password-user';
import { ProfileUpdateUserPage } from '../pages/user/profile-user/profile-update-user/profile-update-user';
import { OrdersUserPage } from '../pages/user/orders-user/orders-user';
import { EventsUserPage } from '../pages/user/events-user/events-user';
import { TipsUserPage } from '../pages/user/tips-user/tips-user';
import { FormTipsUserPage } from '../pages/user/tips-user/form-tips-user/form-tips-user';
import { LeadearboardUserPage } from '../pages/user/leadearboard-user/leadearboard-user';
import { SendMessagesClientPage } from '../pages/client/messages-client/send-messages-client/send-messages-client';
import { MessagesUserClientPage } from '../pages/client/messages-client/messages-user-client/messages-user-client';
import { SendMessagesUserClientPage } from '../pages/client/messages-client/messages-user-client/messages-user-send-client/messages-user-send-client';
import { MessagesUserPage } from '../pages/user/messages-user/messages-user';
import { MessagesClientUserPage } from '../pages/user/messages-user/messages-client-user/messages-client-user';
import { SendMessagesClientUserPage } from '../pages/user/messages-user/messages-client-user/messages-client-send-user/messages-client-send-user';
import { SeeProductsUserPage } from '../pages/user/products-user/see-products-user/see-products-user';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { ProductsFormUserPage } from '../pages/user/products-user/form-products-user/form-products-user';
import { ProductsFormUpdateUserPage } from '../pages/user/products-user/form-products-update-user/form-products-update-user';
import { CategoryFormUserPage } from '../pages/user/categorys-user/form-category-user/form-category-user';
import { PersonalizationUserPage } from '../pages/user/personalization-user/personalization-user';
import { PagaditoAdminPage } from '../pages/client/orders-client/pagadito-client';
import { DetailOrdersClientPage } from '../pages/client/orders-client/detail-orders-client/detail-orders-client';
import { FormEventsUserPage } from '../pages/user/events-user/form-events-user/form-events-user';
import { MessagesTabPage } from '../pages/user/messages-user/messages-tab';
import { MessagesContactsUserPage } from '../pages/user/messages-user/messages-contacts-user/messages-contacts-user';
import { NewsUserPage } from '../pages/user/news-user/news-user';
import { FormNewsUserPage } from '../pages/user/news-user/form-news-user/form-news-user';
import { SeeNewsUserPage } from '../pages/user/news-user/see-news-user/see-news-user';
import { NewsClientPage } from '../pages/client/news-client/news-client';
import { SeeNewsClientCommentPage } from '../pages/client/news-client/see-news-client-comment/see-news-client-comment';
import { SeeNewsClientPage } from '../pages/client/news-client/see-news-client/see-news-client';
import { ChangePasswordModalUserPage } from '../pages/user/change-password-modal/change-password-modal';
import { SeeTipsClientCommentPage } from '../pages/client/tips-client/see-tips-client-comment/see-tips-client-comment';
import { SeeTipsClientPage } from '../pages/client/tips-client/see-tips-client/see-tips-client';
import { SeeTipsUserPage } from '../pages/user/tips-user/see-tips-user/see-tips-user';
import { YoutubeProvider } from '../providers/youtube/youtube';
import { AdmobProvider } from '../providers/admob/admob';
import { StorageProvider } from '../providers/storage/storage';
import { UtilsProvider } from '../providers/utils/utils';
import { YoutubeClientPage } from '../pages/client/youtube-client/youtube-client';
import { SeeYoutubeClientPage } from '../pages/client/youtube-client/see-youtube-client/see-youtube-client';
import { SanitizerPipe } from '../pipes/sanitizer/sanitizer';
import { YoutubeUserPage } from '../pages/user/youtube-user/youtube-user';
import { SeeYoutubeUserPage } from '../pages/user/youtube-user/see-youtube-user/see-youtube-user';
import { ModulesUserPage } from '../pages/user/profile-user/modules-user/modules-user';
import { FormModulesUserPage } from '../pages/user/profile-user/modules-user/form-modules-user/form-modules-user';
import { AccessUserPage } from '../pages/user/profile-user/access-user/access-user';
import { FormChallengeUserPage } from '../pages/user/leadearboard-user/form-challenge-user/form-challenge-user';
import { FormMotivationUserPage } from '../pages/user/leadearboard-user/form-motivation-user/form-motivation-user';
import { BlogClientPage } from '../pages/client/blog-client/blog-client';
import { BlogUserPage } from '../pages/user/blog-user/blog-user';
import { WorkoutsUserPage } from '../pages/user/workouts-user/workouts-user';
import { FormBlogUserPage } from '../pages/user/blog-user/form-blog-user/form-blog-user';
import { FormWorkoutsUserPage } from '../pages/user/workouts-user/form-workouts-user/form-workouts-user';
import { PromotionUserPage } from '../pages/user/promotion-user/promotion-user';
import { WorkoutsClientPage } from '../pages/client/workouts-client/workouts-client';
import { Autosize } from '../pipes/autozise';
import { AdoptionUserPage } from '../pages/user/adoption-user/adoption-user';
import { AdoptionClientPage } from '../pages/client/adoption-client/adoption-client';
import { FormAdoptionDogClientPage } from '../pages/client/adoption-client/form-adoption-dog-client/form-adoption-dog-client';
import { FormAdoptionCatClientPage } from '../pages/client/adoption-client/form-adoption-cat-client/form-adoption-cat-client';
import { MoreConfigurationUserPage } from '../pages/user/profile-user/more-configurations-user/more-configurations-user';
import { ActivePauseUserPage } from '../pages/user/active-pause-user/active-pause-user';
import { FormActivePauseUserPage } from '../pages/user/active-pause-user/form-active-pause-user/form-active-pause-user';
import { SocialUserPage } from '../pages/user/social-user/social-user';
import { FormSocialUserPage } from '../pages/user/social-user/form-social-user/form-social-user';
import { DetailAdoptionDogUserPage } from '../pages/user/adoption-user/detail-adoption-dog-user/detail-adoption-dog-user';
import { DetailAdoptionCatUserPage } from '../pages/user/adoption-user/detail-adoption-cat-user/detail-adoption-cat-user';
import { DetailWorkoutsClientPage } from '../pages/client/workouts-client/detail-workouts-client/detail-workouts-client';
import { CommentsWorkoutsClientPage } from '../pages/client/workouts-client/comments-workouts-client/comments-workouts-client';
import { ActivePauseClientPage } from '../pages/client/active-pause-client/active-pause-client';
import { DetailActivePauseClientPage } from '../pages/client/active-pause-client/detail-active-pause-client/detail-active-pause-client';
import { SocialClientPage } from '../pages/client/social-client/social-client';
import { DetailSocialClientPage } from '../pages/client/social-client/detail-social-client/detail-social-client';
import { DetailBlogClientPage } from '../pages/client/blog-client/detail-blog-client/detail-blog-client';
import { CommentSocialClientPage } from '../pages/client/social-client/comment-social-client/comment-social-client';
import { RunningClubUserPage } from '../pages/user/running-club-user/running-club-user';
import { FormRunningClubUserPage } from '../pages/user/running-club-user/form-running-club-user/form-running-club-user';
import { BenefitsService } from './service/benefits.service';
import { BenefitsUserPage } from '../pages/user/benefits-user/benefits-user';
import { FormBenefitsUserPage } from '../pages/user/benefits-user/form-benefits-user/form-benefits-user';
import { ListYoutubeUserPage } from '../pages/user/youtube-user/list-youtube-user/list-youtube-user';
import { ListYoutubeClientPage } from '../pages/client/youtube-client/list-youtube-client/list-youtube-client';
import { FormCommerceUserPage } from '../pages/user/benefits-user/form-commerce-user/form-commerce-user';
import { CommerceService } from './service/commerce.service';
import { PromotionService } from './service/promotion.service';
import { FormPromotionUserPage } from '../pages/user/promotion-user/form-promotion-user/form-promotion-user';
import { DetailChallengeUserPage } from '../pages/client/leadearboard-client/detail-challenges-client/detail-challenges-client';
import { CommentsChallengeClientPage } from '../pages/client/leadearboard-client/comments-challenges-client/comments-challenges-client';
import { CommentsChallengeUserPage } from '../pages/user/leadearboard-user/comments-challenge-user/comments-challenge-user';
import { PromotionClientPage } from '../pages/client/promotion-client/promotion-client';
import { DetailPromotionClientPage } from '../pages/client/promotion-client/detail-promotion-client/detail-promotion-client';
import { DetailRunningClubClientPage } from '../pages/client/running-club-client/detail-running-club-client/detail-running-club-client';
import { RunningClubClientPage } from '../pages/client/running-club-client/running-club-client';
import { MyMembershipClientPage } from '../pages/client/my-membership-client/my-membership-client';
import { DetailMotivationUserPage } from '../pages/client/leadearboard-client/detail-motivations-client/detail-motivations-client';
import { BenefitsClientPage } from '../pages/client/benefits-client/benefits-client';
import { DetailBenefitsClientPage } from '../pages/client/benefits-client/detail-benefits-client/detail-benefits-client';
import { MyProgressClientPage } from '../pages/client/my-progress-client/my-progress-client';
import { ProgressUserPage } from '../pages/user/progress-user/progress-user';
import { ZFitClubClientPage } from '../pages/client/zfit-club-client/zfit-club-client';
import { FormZFitClubClientPage } from '../pages/client/zfit-club-client/form-zfit-club-client/form-zfit-club-client';
import { CommentZFitClubClientPage } from '../pages/client/zfit-club-client/comment-zfit-club-client/comment-zfit-club-client';
import { DetailZFitClubClientPage } from '../pages/client/zfit-club-client/detail-zfit-club-client/detail-zfit-club-client';
import { ZFitClubUserPage } from '../pages/user/zfit-club-user/zfit-club-user';
import { DetailZFitClubUserPage } from '../pages/user/zfit-club-user/detail-zfit-club-user/detail-zfit-club-user';
import { CommentZFitClubUserPage } from '../pages/user/zfit-club-user/comment-zfit-club-user/comment-zfit-club-user';
import { DetailCommerceClientPage } from '../pages/client/benefits-client/detail-commerce-client/detail-commerce-client';
import { StaffUserPage } from '../pages/user/staff-user/staff-user';
import { StaffService } from './service/staff.service';
import { FormStaffUserPage } from '../pages/user/staff-user/form-staff-user/form-staff-user';
import { StaffClientPage } from '../pages/client/staff-client/staff-client';
import { ProgressService } from './service/progress.service';
import { FormProgressUserPage } from '../pages/user/progress-user/form-progress-user/form-progress-user';
import { AdoptionCatsClientPage } from '../pages/client/adoption-client/adoption-cats-client/adoption-cats-client';
import { AdoptionDogsClientPage } from '../pages/client/adoption-client/adoption-dogs-client/adoption-dogs-client';
import { AdoptionCatsUserPage } from '../pages/user/adoption-user/adoption-cats-user/adoption-cats-user';
import { AdoptionDogsUserPage } from '../pages/user/adoption-user/adoption-dogs-user/adoption-dogs-user';
import { MotivationsUserPage } from '../pages/user/motivations-user/motivations-user';
import { FormMotivationsUserPage } from '../pages/user/motivations-user/form-motivations-user/form-motivations-user';
import { DemoMapsPage } from '../pages/user/demo-maps/demo-maps';
import { EventsTypeService } from './service/events-type.service';
import { FormEventTypeUserPage } from '../pages/user/zfit-club-user/form-event-type-user/form-event-type-user';
import { FormDemoMapsPage } from '../pages/user/demo-maps/form-demo-maps/form-demo-maps';
import { DetailMotivationsUserPage } from '../pages/client/motivation-client/detail-motivation-client/detail-motivation-client';
import { MotivationClientPage } from '../pages/client/motivation-client/motivation-client';
import { MembershipService } from './service/membership.service';
import { DetailOrdersUserPage } from '../pages/user/orders-user/detail-orders-user/detail-orders-user';
import { TermsAndConditionsPage } from '../pages/register/terms-and-conditions/terms-and-conditions';
import { MembershipUserPage } from '../pages/user/membership-user/membership-user';
import { DetailMembershipUserPage } from '../pages/user/membership-user/detail-membership-user/detail-membership-user';
import { CobradoresPage } from '../pages/user/cobradores/cobradores';
import { FormCobradoresPage } from '../pages/user/cobradores/form-cobradores/form-cobradores';
import { CobrosPage } from '../pages/user/cobros/cobros';
import { FormCobrosPage } from '../pages/user/cobros/form-cobros/form-cobros';

@NgModule({
  declarations: [
    SanitizerPipe,
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    TermsAndConditionsPage,
    ResetPasswordPage,
    //ADMINISTRADOR
    AdministratorPage,
    ProductsAdminPage,
      ProductsFormAdminPage,
      ProductsFormUpdateAdminPage,
      SeeProductsAdminPage,
    EventsAdminPage,
      EventsFormAdminPage,
      EventsFormUpdateAdminPage,
      SeeEventsAdminPage,
    ProfileAdminPage,
      ChangePasswordProfileAdminPage,
      ProfileUpdateAdminPage,
    //CLIENTE
    ClientPage,
    CategorysClientPage,
    ProductsClientPage,
      SeeProductsClientPage,
      SeeProductsClientCommentPage,
    EventsClientPage,
      SeeEventsClientPage,
      SeeEventsClientCommentPage,
    OrdersClientPage,
    MessagesClientPage,
      SendMessagesClientPage,
      MessagesUserClientPage,
      SendMessagesUserClientPage,
    MyOrdersClientPage,
    NewsClientPage,
      SeeNewsClientPage,
      SendMessagesClientUserPage,
    TipsClientPage,
      FormTipsClientPage,
      SeeTipsClientCommentPage,
      SeeTipsClientPage,
    ProfileClientPage,
      ChangePasswordProfileClientPage,
      ProfileUpdateClientPage,
      ChangePasswordModalUserPage,
    PersonalizationClientPage,
    PagaditoAdminPage,
    LeadearboardClientPage,
      DetailChallengeUserPage,
      CommentsChallengeClientPage,
      DetailMotivationUserPage,
    BlogClientPage,
      DetailBlogClientPage,
    WorkoutsClientPage,
      DetailWorkoutsClientPage,
      CommentsWorkoutsClientPage,
    AdoptionClientPage,
      AdoptionCatsClientPage,
      AdoptionDogsClientPage,
      FormAdoptionDogClientPage,
      FormAdoptionCatClientPage,
    ActivePauseClientPage,
      DetailActivePauseClientPage,
    SocialClientPage,
      DetailSocialClientPage,
      CommentSocialClientPage,
    PromotionClientPage,
      DetailPromotionClientPage,
    RunningClubClientPage,
      DetailRunningClubClientPage,
    MyMembershipClientPage,
    BenefitsClientPage,
      DetailBenefitsClientPage,
      DetailCommerceClientPage,
    MyProgressClientPage,
    ZFitClubClientPage,
      FormZFitClubClientPage,
      CommentZFitClubClientPage,
      DetailZFitClubClientPage,
    StaffClientPage,
    MotivationClientPage,
      DetailMotivationsUserPage,
    //USUARIO
    UserPage,
    CategorysUserPage,
      CategoryFormUserPage,
    ProductsUserPage,
      SeeProductsUserPage,
      ProductsFormUserPage,
      ProductsFormUpdateUserPage,
    OrdersUserPage,
      DetailOrdersUserPage,
    MembershipUserPage,
      DetailMembershipUserPage,
    EventsUserPage,
      FormEventsUserPage,
    NewsUserPage,
      FormNewsUserPage,
      SeeNewsClientCommentPage,
      SeeNewsUserPage,
    TipsUserPage,
      FormTipsUserPage,
      SeeTipsUserPage,
    MessagesUserPage,
      MessagesClientUserPage,
      MessagesTabPage,
      MessagesContactsUserPage,
    ProfileUserPage,
      ChangePasswordProfileUserPage,
      ProfileUpdateUserPage,
      ModulesUserPage,
      FormModulesUserPage,
      AccessUserPage,
      MoreConfigurationUserPage,
    PersonalizationUserPage,
    DetailOrdersClientPage,
    LeadearboardUserPage,
      FormChallengeUserPage,
      FormMotivationUserPage,
      CommentsChallengeUserPage,
    BlogUserPage,
      FormBlogUserPage,
    WorkoutsUserPage,
      FormWorkoutsUserPage,
      CommentsWorkoutsClientPage,
    PromotionUserPage,
      FormPromotionUserPage,
    AdoptionUserPage,
      AdoptionCatsUserPage,
      AdoptionDogsUserPage,
      DetailAdoptionDogUserPage,
      DetailAdoptionCatUserPage,
    ActivePauseUserPage,
      FormActivePauseUserPage,
    SocialUserPage,
      FormSocialUserPage,
    SocialClientPage,
      DetailSocialClientPage,
    RunningClubUserPage,
      FormRunningClubUserPage,
    BenefitsUserPage,
      FormBenefitsUserPage,
      FormCommerceUserPage,
    ProgressUserPage,
      FormProgressUserPage,
    ZFitClubUserPage,
      DetailZFitClubUserPage,
      CommentZFitClubUserPage,
      FormEventTypeUserPage,
    StaffUserPage,
      FormStaffUserPage,
    MotivationsUserPage,
      FormMotivationsUserPage,
    DemoMapsPage,
    //YOUTUBE
    YoutubeClientPage,
    YoutubeUserPage,
    SeeYoutubeClientPage,
    SeeYoutubeUserPage,
    ListYoutubeUserPage,
    ListYoutubeClientPage,
    CobrosPage,
    FormCobrosPage,
    FormDemoMapsPage,
    Autosize,
    CobradoresPage,
    FormCobradoresPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicImageViewerModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    FormDemoMapsPage,
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    TermsAndConditionsPage,
    ResetPasswordPage,
    DemoMapsPage,
    //ADMINISTRADOR
    AdministratorPage,CobradoresPage,
    FormCobradoresPage,
    CobrosPage,
    FormCobrosPage,
    ProductsAdminPage,
      ProductsFormAdminPage,
      ProductsFormUpdateAdminPage,
      SeeProductsAdminPage,
    EventsAdminPage,
      EventsFormAdminPage,
      EventsFormUpdateAdminPage,
      SeeEventsAdminPage,
    ProfileAdminPage,
      ChangePasswordProfileAdminPage,
      ProfileUpdateAdminPage,
    //CLIENTE
    ClientPage,
    CategorysClientPage,
    ProductsClientPage,
      SeeProductsClientPage,
      SeeProductsClientCommentPage,
    EventsClientPage,
      SeeEventsClientPage,
      SeeEventsClientCommentPage,
    OrdersClientPage,
    MessagesClientPage,
      SendMessagesClientPage,
      MessagesUserClientPage,
      SendMessagesUserClientPage,
    MyOrdersClientPage,
    NewsClientPage,
      SeeNewsClientPage,
      SendMessagesClientUserPage,
    TipsClientPage,
      FormTipsClientPage,
      SeeTipsClientCommentPage,
      SeeTipsClientPage,
    ProfileClientPage,
      ChangePasswordProfileClientPage,
      ProfileUpdateClientPage,
      ChangePasswordModalUserPage,
    PersonalizationClientPage,
    PagaditoAdminPage,
    LeadearboardClientPage,
      DetailChallengeUserPage,
      CommentsChallengeClientPage,
      DetailMotivationUserPage,
    BlogClientPage,
      DetailBlogClientPage,
    WorkoutsClientPage,
      DetailWorkoutsClientPage,
      CommentsWorkoutsClientPage,
    ActivePauseClientPage,
      DetailActivePauseClientPage,
    AdoptionClientPage,
      AdoptionCatsClientPage,
      AdoptionDogsClientPage,
      FormAdoptionDogClientPage,
      FormAdoptionCatClientPage,
    SocialClientPage,
      DetailSocialClientPage,
      CommentSocialClientPage,
    PromotionClientPage,
      DetailPromotionClientPage,
    RunningClubClientPage,
      DetailRunningClubClientPage,
    MyMembershipClientPage,
    BenefitsClientPage,
      DetailBenefitsClientPage,
      DetailCommerceClientPage,
    MyProgressClientPage,
    ZFitClubClientPage,
      FormZFitClubClientPage,
      CommentZFitClubClientPage,
      DetailZFitClubClientPage,
    StaffClientPage,
    MotivationClientPage,
      DetailMotivationsUserPage,
    //USUARIO
    UserPage,
    CategorysUserPage,
      CategoryFormUserPage,
    ProductsUserPage,
      SeeProductsUserPage,
      ProductsFormUserPage,
      ProductsFormUpdateUserPage,
    OrdersUserPage,
      DetailOrdersUserPage,
    EventsUserPage,
      FormEventsUserPage,
    MembershipUserPage,
      DetailMembershipUserPage,
    NewsUserPage,
      FormNewsUserPage,
      SeeNewsUserPage,
      SeeNewsClientCommentPage,
    TipsUserPage,
      FormTipsUserPage,
      SeeTipsUserPage,
    MessagesUserPage,
      MessagesClientUserPage,
      MessagesTabPage,
      MessagesContactsUserPage,
    ProfileUserPage,
      ChangePasswordProfileUserPage,
      ProfileUpdateUserPage,
      ModulesUserPage,
      FormModulesUserPage,
      AccessUserPage,
      MoreConfigurationUserPage,
    PersonalizationUserPage,
    DetailOrdersClientPage,
    LeadearboardUserPage,
      FormChallengeUserPage,
      FormMotivationUserPage,
      CommentsChallengeUserPage,
    BlogUserPage,
      FormBlogUserPage,
    WorkoutsUserPage,
      FormWorkoutsUserPage,
    PromotionUserPage,
      FormPromotionUserPage,
    AdoptionUserPage,
      AdoptionCatsUserPage,
      AdoptionDogsUserPage,
      DetailAdoptionDogUserPage,
      DetailAdoptionCatUserPage,
    ActivePauseUserPage,
      FormActivePauseUserPage,
    SocialUserPage,
      FormSocialUserPage,
    RunningClubUserPage,
      FormRunningClubUserPage,
    BenefitsUserPage,
      FormBenefitsUserPage,
      FormCommerceUserPage,
    ProgressUserPage,
      FormProgressUserPage,
    ZFitClubUserPage,
      DetailZFitClubUserPage,
      CommentZFitClubUserPage,
      FormEventTypeUserPage,
    StaffUserPage,
      FormStaffUserPage,
    MotivationsUserPage,
      FormMotivationsUserPage,
    //YOUTUBE
    YoutubeClientPage,
    YoutubeUserPage,
    SeeYoutubeClientPage,
    SeeYoutubeUserPage,
    ListYoutubeUserPage,
    ListYoutubeClientPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //SERVICIOS
    AuthService,
    CategorysService,
    ProductsService,
    UsersTypesService,
    UsersService,
    EventsService,
    UserEventsService,
    OrdersService,
    MessagesService,
    GoogleMaps,
    NewsService,
    TipsService,
    AccessService,
    ModulesService,
    MotivationService,
    ChallengeService,
    BlogService,
    WorkoutsService,
    SocialService,
    ActivePauseService,
    AdoptionDogsService,
    AdoptionCatsService,
    BenefitsService,
    CommerceService,
    PromotionService,
    StaffService,
    ProgressService,
    EventsTypeService,
    MembershipService,
    Geolocation,
    InAppBrowser,
    //StreamingMedia,
    PhotoViewer,
    //UPLOAD PICTURE
    FileTransfer,
    FileTransferObject,
    File,
    //YOUTUBE
    AdMobFree,
    HttpClient,
    YoutubeProvider,
    //ONE SIGNAL
    //OneSignal,
    AdmobProvider,
    StorageProvider,
    UtilsProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
