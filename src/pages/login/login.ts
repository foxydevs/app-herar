import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

//IMPORTE DE DASHBOARD ADMIN, CLIENTE, USUARIO
import { AdministratorPage } from '../administrator/administrator';
import { UserPage } from '../user/user';
//import { DashboardUserPage } from '../user/dashboard-user/dashboard-user';

//IMPORTAR SERVICIOS
import { AuthService } from '../../app/service/auth.service';
import { UsersService } from '../../app/service/users.service';
import { RegisterPage } from '../register/register';
import { ClientPage } from '../client/client';
import { ResetPasswordPage } from '../reset-password/reset-password';
import { path } from "./../../app/config.module";

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginPage {
  //PROPIEDADES
  private btnDisabled:boolean;
  private baseId:number = path.id;
  private baseUser:any;
  private pictureLogin:string = localStorage.getItem('currentPictureLogin');
  private user:any = {
    username:"",
    password: ""
  }
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public authentication: AuthService,
    public usersService: UsersService) {
      this.btnDisabled = false;
      this.loadSingleUser();
  }

  public loadSingleUser(){

      if((this.baseId+'')!='null'){
        this.usersService.getSingle(this.baseId)
        .then(response => {
          this.baseUser = response;
          this.pictureLogin = response.pic1.toString();
          localStorage.setItem('currentPictureLogin', response.pic1);
          localStorage.setItem('currentpictureCover', response.pic3);
          localStorage.setItem('currentPictureCategories', response.pic2);
          localStorage.setItem('currentPicture11', response.picture);
          localStorage.setItem('currentIdYoutube', response.facebook_id);
          localStorage.setItem('currentColor', response.color);
          localStorage.setItem('currentColorButton', response.color_button);
          localStorage.setItem('currentHome', response.opcion2);
          this.btnColor = response.color_button;
          if(response.opcion1) {
            localStorage.setItem('currentCurrency', response.opcion1);
          } else {
            localStorage.setItem('currentCurrency', 'Q');            
          }
          //console.clear();
        }).catch(error => {
          localStorage.removeItem('currentPictureLogin');
          localStorage.removeItem('currentPictureLogin');
          localStorage.removeItem('currentpictureCover');
          localStorage.removeItem('currentPictureCategories');
          localStorage.removeItem('currentPicture11');
          localStorage.removeItem('currentIdYoutube');
          //console.clear();
        })
      }else{
        localStorage.removeItem('currentPictureLogin');
        localStorage.removeItem('currentPictureLogin');
        localStorage.removeItem('currentpictureCover');
        localStorage.removeItem('currentPictureCategories');
        localStorage.removeItem('currentPicture11');
        localStorage.removeItem('currentIdYoutube');
      }
  }

  public logIn() {
    if(this.user.username || this.user.password) {
      if(this.user.username) {
        if(this.user.password) {
          this.btnDisabled = true;
          if(this.baseId>0){
            this.authentication.authenticationID(this.user,this.baseId)
            .then(response => {
               console.log(response);
              if(response.state > 0) {
                let type:string = null;
                localStorage.setItem('currentUser', response.username);
                localStorage.setItem('currentEmail', response.email);
                localStorage.setItem('currentId', response.id);
                localStorage.setItem('currentState', response.estado);                
                localStorage.setItem('currentPicture', response.picture);
                localStorage.setItem('currentFirstName', response.firstname);
                localStorage.setItem('currentLastName', response.lastname);
                localStorage.setItem('currentState', response.state);
                localStorage.setItem('currentMembresia', response.tipoNivel);
                localStorage.setItem('currentMembresiaFin', response.finMembresia)
                switch(response.type) {
                  case 1:{
                    if(response.id == this.baseId){
                      type = 'user';
                      this.loading.create({
                        content: "Iniciando Sesión Usuario...",
                        duration: 500
                      }).present();
                      localStorage.setItem('currentRolId', response.type);
                      this.navCtrl.setRoot(UserPage);
                      break;
                    } else {
                      type = 'client';
                      this.loading.create({
                        content: "Iniciando Sesión...",
                        duration: 500
                      }).present();
                      localStorage.setItem('currentRolId', '2');
                      this.navCtrl.setRoot(ClientPage);
                      break;
                    }
                  }
                  case 2:{
                    type = 'client';
                    this.loading.create({
                      content: "Iniciando Sesión...",
                      duration: 500
                    }).present();
                    localStorage.setItem('currentRolId', response.type);
                    this.navCtrl.setRoot(ClientPage);
                    break;
                  }
                  /*case 4:{
                    type = 'admin';
                    this.loading.create({
                      content: "Iniciando Sesión Administrador...",
                      duration: 500
                    }).present();
                    this.navCtrl.setRoot(AdministratorPage);
                    break;
                  }*/
                  default:{
                    type = 'usuario';
                    break;
                  }
                }
                localStorage.setItem('currentType', type);
                //console.clear();
                this.btnDisabled = false;

              } else {
                this.btnDisabled = false;
                this.toast.create({
                  message: "Su usuario se encuentra deshabilitado temporalmente.",
                  duration: 3000
                }).present();
              }
            }).catch(error => {
              //console.clear();
              this.btnDisabled = false;
              if(error.status == 401){
                this.btnDisabled = false;
                this.toast.create({
                  message: "Usuario o contraseña incorrectos.",
                  duration: 3000
                }).present();
              }else
              if(error.status == 400){
                this.btnDisabled = false;
                this.toast.create({
                  message: "Su usuario no esta registrado en esta Aplicacion.",
                  duration: 3000
                }).present();
              }
            });
          }else{
            this.authentication.authentication(this.user)
            .then(response => {
              // console.log(response);
              if(response.state > 0) {
                let type:string = null;
                localStorage.setItem('currentUser', response.username);
                localStorage.setItem('currentEmail', response.email);
                localStorage.setItem('currentId', response.id);
                localStorage.setItem('currentState', response.estado);
                localStorage.setItem('currentRolId', response.type);
                localStorage.setItem('currentPicture', response.picture);
                localStorage.setItem('currentFirstName', response.firstname);
                localStorage.setItem('currentLastName', response.lastname);
                localStorage.setItem('currentMembresia', response.tipoNivel);
                localStorage.setItem('currentMembresiaFin', response.finMembresia)
                switch(response.type) {
                  case 1:{
                    type = 'user';
                    this.loading.create({
                      content: "Iniciando Sesión Usuario...",
                      duration: 500
                    }).present();
                    this.navCtrl.setRoot(UserPage);
                    break;
                  }
                  case 2:{
                    type = 'client';
                    this.loading.create({
                      content: "Iniciando Sesión...",
                      duration: 500
                    }).present();
                    this.navCtrl.setRoot(ClientPage);
                    break;
                  }
                  case 4:{
                    type = 'admin';
                    this.loading.create({
                      content: "Iniciando Sesión Administrador...",
                      duration: 500
                    }).present();
                    this.navCtrl.setRoot(AdministratorPage);
                    break;
                  }
                  default:{
                    type = 'usuario';
                    break;
                  }
                }
                localStorage.setItem('currentType', type);
                //console.clear();
                this.btnDisabled = false;
              } else {
                this.btnDisabled = false;
                this.toast.create({
                  message: "Su usuario se encuentra deshabilitado temporalmente.",
                  duration: 3000
                }).present();
              }
            }).catch(error => {
              //console.clear();
              if(error.status == 401){
                this.btnDisabled = false;
                this.toast.create({
                  message: "Usuario o contraseña incorrectos.",
                  duration: 3000
                }).present();
              }
            });
          }

        } else {
          this.toast.create({
            message: "Ingrese una contraseña.",
            duration: 3000
          }).present();
        }
      } else {
        this.toast.create({
          message: "Ingrese un usuario.",
          duration: 3000
        }).present();
      }
    } else {
      this.toast.create({
        message: "Ingrese un usuario y contraseña.",
        duration: 3000
      }).present();
    }
  }

  public createAccount() {
    this.navCtrl.push(RegisterPage);
  }

  public resetPassword() {
    this.navCtrl.push(ResetPasswordPage);
  }

}

