import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Component({
  selector: 'change-password-modal',
  templateUrl: 'change-password-modal.html'
})
export class ChangePasswordModalUserPage {
  //PROPIEDADES
  private changePassword = {
    old_pass: '',
  	new_pass : '',
	  new_pass_rep: '',
    id: ''
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  constructor(
    public navCtrl: NavController,
    public usersService: UsersService,
    public toast: ToastController,
    public loading: LoadingController,
    public view: ViewController,
    public alertCtrl: AlertController,
  ) {
    this.changePassword.id = localStorage.getItem('currentId')
  }

  //ACTUALIZAR PASSWORD
  public updatePassword() {
    if(this.changePassword.old_pass == this.changePassword.new_pass) {
      this.message('No puede usar la misma contraseña.');
    } else if(this.changePassword.new_pass.length < 8) {
      this.message('La contraseña debe contener al menos 8 caracteres.');
    } else if(this.changePassword.new_pass.length >= 8) {
      if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
        this.loading.create({
          content: "Cambiando Contraseña...",
          duration: 1000
        }).present();
        this.usersService.changePassword(this.changePassword)
        .then(response => {
          this.view.dismiss();
          this.confirmation('Cambio de Contraseña', 'Su contraseña ha sido modificada exitosamente.')
          localStorage.removeItem('currentState');
        }).catch(error => {
          this.message('Contraseña Inválida.');
        })
      } else {
        this.message('Las contraseñas no coinciden.');
      }
    } else {
      this.message('Las contraseñas no coinciden.');
    }
  }

  //CONFIRMACION
  public confirmation(title: any, message?:any) {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss();
  }

}
