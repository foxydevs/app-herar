import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { FormWorkoutsUserPage } from './form-workouts-user/form-workouts-user';
import { WorkoutsService } from '../../../app/service/workouts.service';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';

@Component({
  selector: 'workouts-user',
  templateUrl: 'workouts-user.html',
})
export class WorkoutsUserPage {
  //PROPIEDADES
  private table:any[] = [];
  private idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: WorkoutsService,
  ) {
    this.idUser = localStorage.getItem('currentId');
  }

  openForm(parameter?:any) {
    this.navCtrl.push(FormWorkoutsUserPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

}
