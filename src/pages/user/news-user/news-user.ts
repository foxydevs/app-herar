import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { NewsService } from '../../../app/service/news.service';
import { FormNewsUserPage } from './form-news-user/form-news-user';
import { SeeNewsUserPage } from './see-news-user/see-news-user';

//JQUERY
declare var $:any;

@Component({
  selector: 'news-user',
  templateUrl: 'news-user.html'
})
export class NewsUserPage {
  //PROPIEDADES
  private news:any[] = [];
  private search:any;
  private idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public newsService: NewsService,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams
  ) {
    this.idUser = localStorage.getItem('currentId');
  }

  //CARGAR PRODUCTOS
  public getAll() {
    this.newsService.getAll()
    .then(response => {
      this.news = [];
      this.news = response;
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  openForm(parameter:any) {
    this.navCtrl.push(FormNewsUserPage, { parameter });
  }

  seeLiveVideo(parameter:any) {
    this.loading.create({
      content: 'Cargando...',
      duration: 1000
    }).present();
    this.navCtrl.push(SeeNewsUserPage, { parameter })   
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
