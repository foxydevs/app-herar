import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';
import { Geolocation } from '@ionic-native/geolocation';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { NewsService } from '../../../../app/service/news.service';
//import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
//import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

@Component({
  selector: 'see-news-user',
  templateUrl: 'see-news-user.html'
})
export class SeeNewsUserPage implements OnInit {
  private users:any[] = [];
  private trustedVideoUrl: SafeResourceUrl;
  private comments:any[] = [];
  private parameter:any;
  private news = {
    title: '',
    description: '',
    link: '',
    picture: '',
    id: '',
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public usersService: UsersService,
    public loading: LoadingController,
    public geolocation: Geolocation,
    public newsService: NewsService,
    public alertCtrl: AlertController,
    //private streamingMedia: StreamingMedia,
    //private youtube: YoutubeVideoPlayer
    private domSanitizer: DomSanitizer
  ) {
    this.loadAllUsers();
    this.parameter = this.navParams.get('parameter');
  }

  ngOnInit() {
  }

  //CARGAR COMENTARIOS POR NOTICIAS
  public loadCommentsByNew(id:any) {
    this.comments = [];
    this.newsService.getAllCommentsByNew(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: x.created_at,
          user: this.returnNameUser(x.user),
          picture: this.returnPicture(x.user)
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //CARGAR USUARIOS
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR LA NOTICIA
  public loadSingleNew(idEvent:any) {
    this.newsService.getSingle(idEvent)
    .then(response => {
      this.news.description = response.description;
      this.news.title = response.title;
      this.news.link = response.link;
      this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(response.link);
      this.news.picture = response.picture;
      this.news.id = response.id;
    }).catch(error => {
      console.clear;
    })
  }

  //RETORNAR NOMBRE DE USUARIO
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //RETORNAR IMAGEN USUARIO
  public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }

  public openPage(parameter:any) {
    //this.navCtrl.push(SeeNewsClientCommentPage, { parameter });
  }

  /*public startVideo() {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Finished Video') },
      errorCallback: (e) => { console.log('Error: ', e) },
      orientation: 'portrait'
    };
 
    this.streamingMedia.playVideo(this.news.link, options);
  }*/

  public videoYoutube() {
    //this.youtube.openVideo(this.news.link);
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.loadSingleNew(this.parameter);
      this.loadCommentsByNew(this.parameter)
    }, 800);
  }

}