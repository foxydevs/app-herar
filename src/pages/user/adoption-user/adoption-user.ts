import { Component } from '@angular/core';
import { AdoptionDogsUserPage } from './adoption-dogs-user/adoption-dogs-user';
import { AdoptionCatsUserPage } from './adoption-cats-user/adoption-cats-user';

@Component({
  templateUrl: 'adoption-user.html'
})
export class AdoptionUserPage {

  tab1Root = AdoptionDogsUserPage;
  tab2Root = AdoptionCatsUserPage;

  constructor() {

  }
}