import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AdoptionCatsService } from '../../../../app/service/adoption-cats.service';
import { DetailAdoptionCatUserPage } from '../detail-adoption-cat-user/detail-adoption-cat-user';

@Component({
  selector: 'adoption-cats-user',
  templateUrl: 'adoption-cats-user.html',
})
export class AdoptionCatsUserPage {
  //PROPIEDADES
  private idUser:any;
  private selectItem:any = 'proceso';
  private adoptionsCats:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public modal: ModalController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: AdoptionCatsService,
  ) {
    this.idUser = localStorage.getItem('currentId')
  }

  openForm(parameter?:any) {
    this.navCtrl.push(DetailAdoptionCatUserPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any, state:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUserState(id, state)
    .then(response => {
      this.adoptionsCats = []
      this.adoptionsCats = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    if(this.selectItem == 'proceso') {
      this.getAll(this.idUser, 2);
      
    } else if(this.selectItem == 'aceptadas') {
      this.getAll(this.idUser, 1);

    } else if(this.selectItem == 'rechazadas') {
      this.getAll(this.idUser, 0);
    }
    //this.getAll(this.idUser);
  }

}
