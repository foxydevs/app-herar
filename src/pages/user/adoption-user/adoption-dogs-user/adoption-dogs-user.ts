import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AdoptionDogsService } from '../../../../app/service/adoption-dogs.service';
import { DetailAdoptionDogUserPage } from '../detail-adoption-dog-user/detail-adoption-dog-user';

@Component({
  selector: 'adoption-dogs-user',
  templateUrl: 'adoption-dogs-user.html',
})
export class AdoptionDogsUserPage {
  //PROPIEDADES
  private idUser:any;
  selectItem:any = 'proceso';
  private adoptionsDogs:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public modal: ModalController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: AdoptionDogsService,
  ) {
    this.idUser = localStorage.getItem('currentId')
  }

  openForm(parameter?:any) {
    this.navCtrl.push(DetailAdoptionDogUserPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any, state:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUserState(id, state)
    .then(response => {
      this.adoptionsDogs = []
      this.adoptionsDogs = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    if(this.selectItem == 'proceso') {
      this.getAll(this.idUser, 2);
      
    } else if(this.selectItem == 'aceptadas') {
      this.getAll(this.idUser, 1);

    } else if(this.selectItem == 'rechazadas') {
      this.getAll(this.idUser, 0);
    }
    //this.getAll(this.idUser);
  }

}
