import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { AdoptionCatsService } from '../../../../app/service/adoption-cats.service';

@Component({
  selector: 'detail-adoption-cat-user',
  templateUrl: 'detail-adoption-cat-user.html',
})
export class DetailAdoptionCatUserPage {
  //PROPIEDADES
  private parameter:any;
  private data:any[] = [];
  private previousNext:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  pregunta25:any;

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public loading: LoadingController,
    public mainService: AdoptionCatsService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getSingle(this.parameter);
    this.previousNext = 'page1'
  }

  public previousNextForm(palabra: any) {
    this.previousNext = palabra;
  }

  //CANCELAR
  cancel(id:string){
    let adoption = {
      state: '0',
      id: id
    }
    let confirm = this.alertCtrl.create({
      title: '¿Deseas cancelar la adopción?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Cancelando..."
            });
            load.present();
            this.mainService.update(adoption)
            .then(response => {
              load.dismiss();
              this.confirmation('Adopción Cancelada', "Se le informara al cliente que su adopción no procede.");
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //ACEPTAR
  accept(id:string){
    let adoption = {
      state: '1',
      id: id
    }
    let confirm = this.alertCtrl.create({
      title: '¿Deseas aceptar la adopción?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Cargando..."
            });
            load.present();
            this.mainService.update(adoption)
            .then(response => {
              load.dismiss();
              this.confirmation('Adopción Autorizada', "Se le informara al cliente que su adopción esta en proceso.");
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //ACEPTAR
  process(id:string){
    let adoption = {
      state: '3',
      id: id,
      pregunta25: this.pregunta25
    }
    let confirm = this.alertCtrl.create({
      title: '¿Deseas solicitar más información para la adopción?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Cargando..."
            });
            load.present();
            this.mainService.update(adoption)
            .then(response => {
              load.dismiss();
              this.confirmation('Adopción en Proceso', "Se le informara al cliente que falta información.");
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      this.pregunta25 = response.pregunta25;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

}
