import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { CategorysService } from '../../../app/service/categorys.service';
import { ProductsUserPage } from '../products-user/products-user';
import { CategoryFormUserPage } from './form-category-user/form-category-user';
import { UsersService } from '../../../app/service/users.service';
import { path } from "./../../../app/config.module";
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { ChangePasswordModalUserPage } from '../change-password-modal/change-password-modal';

@Component({
  selector: 'categorys-user',
  templateUrl: 'categorys-user.html'
})
export class CategorysUserPage {
  //Propiedades
  private categorys:any[] = [];
  private idUser:any;
  private pictureCategories:any;
  private baseId = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  constructor(
    public navCtrl: NavController,
    public categorysService: CategorysService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public usersService: UsersService,
    public modal: ModalController,
  ) {
    this.idUser = localStorage.getItem("currentId");
    if(localStorage.getItem('currentState') == '21') {
      this.openModalCreate();
    }
    this.loadSingleUser();
    this.loadAll();
  }

  public loadSingleUser(){
    if((this.baseId+'')!='null'){
      this.usersService.getSingle(this.baseId)
      .then(response => {
        this.pictureCategories = response.pic2
        //console.clear();
      }).catch(error => {
        //console.clear();
      })
    }
  }

  //Cargar los productos
  public loadAll(){
    this.categorysService.getAllUser(this.idUser)
    .then(response => {
      this.categorys = []
      this.categorys = response;
    }).catch(error => {
      console.clear
    })
  }

  //Ver Productos de la Categoria
  public seeProducts(parameter:any) {
    this.loading.create({
        content: "Cargando",
        duration: 750
    }).present();
    this.navCtrl.push(ProductsUserPage, { parameter });
  }

  //Ver Formulario Agregar
  public viewForm() {
    let parameter:string = 'new';
    this.navCtrl.push(CategoryFormUserPage, { parameter });
  }

  public updateForm(parameter:any) {
    this.navCtrl.push(CategoryFormUserPage, { parameter });
  }

  //Eliminar Productos
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la categoría?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.categorysService.delete(id)
            .then(response => {
                this.loading.create({
                  content: "Eliminando Categoría...",
                  duration: 2000
                }).present();
                this.loadAll();
                console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  //ABIR MODAL AGREGAR
  public openModalCreate() {
    this.loading.create({
      content: "Cargando...",
      duration: 200
    }).present();
    let chooseModal = this.modal.create(ChangePasswordModalUserPage);
    chooseModal.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadSingleUser();
      this.loadAll();
      refresher.complete();
    }, 2000);
  }

}
