import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { path } from "../../../app/config.module";
import { UsersService } from '../../../app/service/users.service';
import { Events } from 'ionic-angular/util/events';

//JQUERY
declare var $:any;

@Component({
  selector: 'personalization-user',
  templateUrl: 'personalization-user.html'
})
export class PersonalizationUserPage {
  //PROPIEDADES
  private basePath:string = path.path;
  private idUser:any;
  private profilePicture1:any;
  private profilePicture2:any;
  private profilePicture3:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public usersService: UsersService,
    public events: Events
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.loadPictures(this.idUser)
  }

  //LOAD PICTURES
  public loadPictures(id:any) {
    this.usersService.getSingle(id)
    .then(response => {
    this.profilePicture1 = response.pic1;
    this.profilePicture2 = response.pic2;
    this.profilePicture3 = response.pic3;
    }).catch(error => {
    })
  }

  //Subir Imagenes de Perfil
  uploadImage1(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic1/${this.idUser}`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(3*(1024*1024))) {
            $('#imgAvatar1').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                    $('#imgAvatar1').attr("src",respuesta.pic1)
                    $("#"+id).val('')
                }
            );
        } else {
          this.messages('La imagen es demasiado grande.');
        }
    } else {
      this.messages('El tipo de imagen no es válido.');
    }
  }

  //Subir Imagenes de Perfil
  uploadImage2(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic2/${this.idUser}`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(3*(1024*1024))) {
            $('#imgAvatar2').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                    $('#imgAvatar2').attr("src",respuesta.pic2)
                    $("#"+id).val('')
                }
            );

        } else {
          this.messages('La imagen es demasiado grande.');
        }
    } else {
      this.messages('El tipo de imagen no es válido.');
    }
  }

  //Subir Imagenes de Perfil
  uploadImage3(archivo, id) {
    let events = this.events;
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic3/${this.idUser}`;
    console.log(archivos)

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(3*(1024*1024))) {
            $('#imgAvatar3').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                  localStorage.setItem('currentPictureCover', respuesta.pic3); 
                  $('#imgAvatar3').attr("src",respuesta.pic3);
                  events.publish('user:updates');
                  $("#"+id).val('')
                }
            );

        } else {
          this.messages('La imagen es demasiado grande.');
        }
    } else {
      this.messages('El tipo de imagen no es válido.');
    }
  }

  //MENSAJES
  public messages(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

}
