import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';
import { SendMessagesClientUserPage } from '../messages-client-user/messages-client-send-user/messages-client-send-user';

@Component({
  selector: 'messages-contacts-user',
  templateUrl: 'messages-contacts-user.html'
})
export class MessagesContactsUserPage {
  //PROPIEDADES
  private idUser:any;
  private users:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public usersService: UsersService
  ) {
    this.idUser = localStorage.getItem("currentId");
    this.loadAllUsers();
  }

  //CARGAR USUARIOS
  public loadAllUsers(){
    this.usersService.getClients(this.idUser)
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear
    })
  }

  public sendMessage(param:any) {
    let parameter = {
      parameter: param,
      message: 'New'
    }
    console.log(parameter)
    this.navCtrl.push(SendMessagesClientUserPage, { parameter });
  }

  ionViewWillEnter() {
    this.loadAllUsers();
  }
}
