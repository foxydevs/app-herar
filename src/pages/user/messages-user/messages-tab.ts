import { Component } from '@angular/core';
import { MessagesUserPage } from './messages-user';
import { MessagesContactsUserPage } from './messages-contacts-user/messages-contacts-user';


@Component({
  templateUrl: 'messages-tab.html'
})
export class MessagesTabPage {

    tab1Root = MessagesUserPage;
    tab2Root = MessagesContactsUserPage;

  constructor() {

  }
}