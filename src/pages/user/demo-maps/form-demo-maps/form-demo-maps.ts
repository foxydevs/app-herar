import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { ActivePauseService } from '../../../../app/service/activepause.service';

@Component({
  selector: 'form-demo-maps',
  templateUrl: 'form-demo-maps.html',
})
export class FormDemoMapsPage {
  navColor = localStorage.getItem('currentColor');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: ActivePauseService
  ) {
  }


}
