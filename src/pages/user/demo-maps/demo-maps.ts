import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { FormDemoMapsPage } from './form-demo-maps/form-demo-maps';
//JQUERY
declare var google;

@Component({
  selector: 'demo-maps',
  templateUrl: 'demo-maps.html'
})
export class DemoMapsPage{
  //NUEVAS PROPIEDADES
  map: any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  myLatLng: any;
  waypoints: any[];

  //PROPIEDADES
  private data = {
    address: '',
    description : '',
    date: '',
    time: '',
    latitude: '',
    longitude: '',
    user_created: +localStorage.getItem('currentId'),
    user_owner: +localStorage.getItem('currentId'),
    place: '',
    tipo: 1,
    place_id:'0',
    id: '',
  }
  data2= {
    end: 'chicago, il',
    start: 'st louis, mo'
  }
  marker:any;
  parameter:any;
  selectItem:any = 'mapa';
  labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  labelIndex = 0;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public geolocation: Geolocation,
    public view: ViewController,
    public alertCtrl: AlertController
  ) {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
    this.parameter = this.navParams.get('parameter');
    this.waypoints = [
      {
        location: { lat: 4.6241329, lng: -74.1768411 },
        stopover: true,
      },
      {
        location: { lat: 4.6247745, lng: -74.1698888 },
        stopover: true,
      },
      {
        location: { lat: 4.6212241, lng: -74.1631081 },
        stopover: true,
      },
      {
        location: { lat: 4.6222508, lng: -74.1667989 },
        stopover: true,
      }
    ];
    this.getPosition()
  }

  getPosition():any{
    this.geolocation.getCurrentPosition().then(resp => {
      this.getMap(resp)
     }).catch(error => {
      console.log('Error getting location', error);
    });
  }

  initializeMap(position: Geoposition) {
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;
    this.data.latitude = latitude.toString();
    this.data.longitude = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    //let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    let myLatLng1 = new google.maps.LatLng({lat: 14.587267, lng: -90.501131});
    let myLatLng2 = new google.maps.LatLng({lat: 14.587991, lng: -90.502541});

    var map = new google.maps.Map(mapEle, {
      zoom: 17,
      center: myLatLng1
    });

    this.addMarker(myLatLng1, map);
    setTimeout(() => {
    this.addMarker(myLatLng2, map);      
    }, 500);

    /*// This event listener calls addMarker() when the map is clicked.
    google.maps.event.addListener(map, 'click', (evt) => {
      this.addMarker(evt.latLng, map);
    });

    // Add a marker at the center of the map.
    this.addMarker(myLatLng, map);*/
  }

  //CARGAR MAPA
  public loadMap() {
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng1 = new google.maps.LatLng({lat: 14.587267, lng: -90.501131});
    let myLatLng2 = new google.maps.LatLng({lat: 14.587991, lng: -90.502541});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng1,
      zoom: 17
    });
    
    var marker;

    marker = new google.maps.Marker({
      map: this.map,
      position: myLatLng1
    });

    this.addMarker(myLatLng1, marker);
    setTimeout(() => {
    this.addMarker(myLatLng2, marker);      
    }, 500);

    /*google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitude = evt.latLng.lat();
      this.data.longitude = evt.latLng.lng();
    });*/

   /* google.maps.event.addListener(marker, 'click', (evt) => {
      console.log(evt.latLng)
      console.log(marker)
      this.addMarker(evt.latLng, marker);
    });*/
  }

  addMarker(location, map) {
    // Add the marker at the clicked location, and add the next-available label
    // from the array of alphabetical characters.
    var marker;
    marker = new google.maps.Marker({
      position: location,
      label: this.labels[this.labelIndex++ % this.labels.length],
      map: map
    });
  }

  initMap() {
    this.directionsService = new google.maps.DirectionsService();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng1 = new google.maps.LatLng({lat: 41.85, lng: -87.65});

    var map = new google.maps.Map(mapEle, {
      zoom: 10,
      center: myLatLng1
    });
    this.directionsDisplay.setMap(map);
    this.calcRoute();
  }

  calcRoute() {
    var start = this.data2.start;
    var end = this.data2.end;
    var request = {
      origin: start,
      destination: end,
      travelMode: 'DRIVING'
    };
    this.directionsService.route(request, function(response, status) {
      console.log(response)
      if (status == 'OK') {
        this.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  };

  calculateAndDisplayRoute(directionsService, directionsDisplay) {
    directionsService.route({
      origin: document.getElementById('start'),
      destination: document.getElementById('end'),
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  getMap(position: Geoposition){
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;
    console.log(latitude, longitude);
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map');
    let panelEle: HTMLElement = document.getElementById('panel');
  
    // create LatLng object
    this.myLatLng = {lat: 4.6212241, lng: -74.1631};
  
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: this.myLatLng,
      zoom: 12
    });
  
    this.directionsDisplay.setMap(this.map);
    this.directionsDisplay.setPanel(panelEle);
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      this.calculateRoute();
    });
  }
  
  private calculateRoute(){
    
    this.bounds.extend(this.myLatLng);
  
    this.waypoints.forEach(waypoint => {
      var point = new google.maps.LatLng(waypoint.location.lat, waypoint.location.lng);
      this.bounds.extend(point);
    });
  
    this.map.fitBounds(this.bounds);
  
    this.directionsService.route({
      origin: new google.maps.LatLng(this.myLatLng.lat, this.myLatLng.lng),
      destination: new google.maps.LatLng(this.myLatLng.lat, this.myLatLng.lng),
      waypoints: this.waypoints,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
      avoidTolls: true
    }, (response, status)=> {
      if(status === google.maps.DirectionsStatus.OK) {
        console.log(response);
        this.directionsDisplay.setDirections(response);
      }else{
        alert('Could not display directions due to: ' + status);
      }
    });  
  
  }

  open() {
    this.navCtrl.push(FormDemoMapsPage)
  }

}
