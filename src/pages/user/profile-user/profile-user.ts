import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChangePasswordProfileUserPage } from './change-password-user/change-password-user';
import { ProfileUpdateUserPage } from './profile-update-user/profile-update-user';
import { UsersService } from '../../../app/service/users.service';
import { ModulesUserPage } from './modules-user/modules-user';
import { AccessUserPage } from './access-user/access-user';
import { MoreConfigurationUserPage } from './more-configurations-user/more-configurations-user';

@Component({
  selector: 'profile-user',
  templateUrl: 'profile-user.html'
})
export class ProfileUserPage {
  //PROPIEDADES
  private profilePicture:any;
  private user:any;
  private email:any;
  private idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  constructor(
    public navCtrl: NavController,
    public userService: UsersService
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.userService.getSingle(this.idUser)
    .then(response => {
    this.profilePicture = response.picture;
    }).catch(error => {
        console.clear()
    })
  }

  //CAMBIAR CONTRASEÑA
  public changePassword() {
    this.navCtrl.push(ChangePasswordProfileUserPage);
  }

  //ACTUALIZAR USUARIO
  public updateUser() {
    this.navCtrl.push(ProfileUpdateUserPage);
  }

  //VER MODULOS
  public modules() {
    this.navCtrl.push(ModulesUserPage);
  }

  //VER ACCESOS
  public access() {
    this.navCtrl.push(AccessUserPage);
  }

  //VER + CONFIGURACIONES
  public more() {
    this.navCtrl.push(MoreConfigurationUserPage);
  }

  ionViewWillEnter() {
    this.profilePicture = localStorage.getItem("currentPicture");
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
    this.navColor = localStorage.getItem('currentColor');
    this.btnColor = localStorage.getItem('currentButtonColor');
  }

}