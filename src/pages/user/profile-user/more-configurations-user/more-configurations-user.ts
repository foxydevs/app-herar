import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';

@Component({
  selector: 'more-configurations-user',
  templateUrl: 'more-configurations-user.html'
})
export class MoreConfigurationUserPage {
  //Propiedades
  private idUser:any;
  private btnDisabled:boolean;
  private data = {
    opcion1: 'Q',
    opcion2: '1',
    color: 'danger',
    color_button: 'danger',
    id: 0
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //Constructor
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public usersService: UsersService,
    public loading: LoadingController,
    public alertCtrl: AlertController
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.getSingle();
  }

  //CARGAR USUARIO
  public getSingle() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.usersService.getSingle(this.idUser)
    .then(response => {
      this.data.id = response.id;
      this.data.color = response.color;
      this.data.color_button = response.color_button;
      this.data.opcion1 = response.opcion1;
      this.data.opcion2 = response.opcion2;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    })
  }

  //Insertar Datos
  public update() {
    console.log(this.data)
    this.btnDisabled = true;
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.usersService.update(this.data)
    .then(response => {  
      console.log(response)
      localStorage.setItem('currentColor', response.color);
      localStorage.setItem('currentColorButton', response.color_button);
      localStorage.setItem('currentHome', response.opcion2);
      localStorage.setItem('currentCurrency', response.opcion1);
      this.navColor = response.color;
      this.btnColor = response.color_button;
      load.dismiss();
      this.confirmation('Configuración Guardada', 'La configuración del usuario ha sido actualizado exitosamente.')
    }).catch(error => {
      this.btnDisabled = false;
      console.log(Response)
    })
  }

  //CONFIRMACION
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

}
