import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, AlertController } from 'ionic-angular';
import { FormModulesUserPage } from './form-modules-user/form-modules-user';
import { AccessService } from '../../../../app/service/access.service';

@Component({
  selector: 'modules-user',
  templateUrl: 'modules-user.html'
})
export class ModulesUserPage {
  private table:any[] = [];
  private userId:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  constructor(public navCtrl: NavController,
  public loading: LoadingController,
  public alertCtrl: AlertController,
  public mainService: AccessService,
  public modal: ModalController) {
    this.userId = localStorage.getItem('currentId');
  }

  //CARGAR ACCESOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    console.log(this.userId)
    this.mainService.getAccess(this.userId)
    .then(res => {
      this.table = [];
      this.table = res.permitidos;
      console.log(this.table)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    })
  }

  //ABIR
  public openForm(parameter:any) {
    this.navCtrl.push(FormModulesUserPage, { parameter })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
