import { Component } from '@angular/core';
import { NavController, ViewController, LoadingController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { ModulesService } from '../../../../app/service/module.service';
import { AccessService } from '../../../../app/service/access.service';

@Component({
  selector: 'access-user',
  templateUrl: 'access-user.html'
})
export class AccessUserPage {
  private userId:any;
  private access:any = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  constructor(public navCtrl: NavController,
  public modulesService: ModulesService,
  public accessService: AccessService,
  public loading:LoadingController,
  public navParams: NavParams,
  public toast: ToastController,
  public view: ViewController) {
    this.userId = localStorage.getItem('currentId');
    this.getAll();
  }

  //CARGAR ACCESOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    console.log(this.userId)
    this.accessService.getAccess(this.userId)
    .then(res => {
      this.access = [];
      this.access = res.permitidos;
      console.log(this.access)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    })
  }

  //ACTUALIZAR MODULO
  public update(id, a, modulo) {
    let access = {
      id: id,
      mostrar: a,
      modulo: modulo,
      usuario: this.userId
    }
    this.accessService.create(access)
    .then(res => {
      console.log(res)
      this.getAll();
    }).catch(error => {
      console.log(error)
    })
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

}
