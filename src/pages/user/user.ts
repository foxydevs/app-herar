import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategorysUserPage } from './categorys-user/categorys-user';
import { ProductsUserPage } from './products-user/products-user';
import { ProfileUserPage } from './profile-user/profile-user';
import { EventsUserPage } from './events-user/events-user';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { PersonalizationUserPage } from './personalization-user/personalization-user';
import { UsersService } from '../../app/service/users.service';
import { path } from "./../../app/config.module";
import { Events } from 'ionic-angular/util/events';
import { MessagesTabPage } from './messages-user/messages-tab';
import { NewsUserPage } from './news-user/news-user';
import { TipsUserPage } from './tips-user/tips-user';
import { LeadearboardUserPage } from './leadearboard-user/leadearboard-user';
import { BlogUserPage } from './blog-user/blog-user';
import { WorkoutsUserPage } from './workouts-user/workouts-user';
import { PromotionUserPage } from './promotion-user/promotion-user';
import { AdoptionUserPage } from './adoption-user/adoption-user';
import { OrdersUserPage } from './orders-user/orders-user';
import { ActivePauseUserPage } from './active-pause-user/active-pause-user';
import { SocialUserPage } from './social-user/social-user';
import { RunningClubUserPage } from './running-club-user/running-club-user';
import { BenefitsUserPage } from './benefits-user/benefits-user';
import { YoutubeUserPage } from './youtube-user/youtube-user';
import { ProgressUserPage } from './progress-user/progress-user';
import { ZFitClubUserPage } from './zfit-club-user/zfit-club-user';
import { StaffUserPage } from './staff-user/staff-user';
import { MotivationsUserPage } from './motivations-user/motivations-user';
import { DemoMapsPage } from './demo-maps/demo-maps';
import { MembershipUserPage } from './membership-user/membership-user';
import { CobradoresPage } from './cobradores/cobradores';

@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})
export class UserPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = CategorysUserPage;
  pages: Array<{icon:string, ios:string, title: string, component: any}>;
  private picture:any;
  private email:any;
  private firstName:any;
  private baseUser:any;
  private baseId:number = path.id;
  private pictureCover:any;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loading: LoadingController,
    public events: Events,
    private usersService: UsersService) {
    //this.initializeApp();

    this.picture = localStorage.getItem("currentPicture");
    this.email = localStorage.getItem("currentEmail");
    this.firstName = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    // console.log(this.baseId)
    this.loadSingleUser();
    // used for an example of ngFor and navigation
    events.subscribe('user:updates', res => {
      this.pictureCover = localStorage.getItem('currentPictureCover');
      this.picture = localStorage.getItem("currentPicture");
      this.email = localStorage.getItem("currentEmail");
      this.firstName = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    });
    if(this.baseId == 49) {
      this.loadModulesZFIT();
    } else {
      this.loadModules();
    }
  }

  loadModules() {
    this.pages = [
      { icon: 'md-apps', ios: 'ios-apps', title: 'Categorias', component: CategorysUserPage },
      { icon: 'md-basket', ios: 'ios-basket', title: 'Productos', component: ProductsUserPage },
      { icon: 'md-book', ios: 'ios-book', title: 'Pedidos', component: OrdersUserPage },
      { icon: 'md-calendar', ios: 'ios-calendar', title: 'Eventos', component: EventsUserPage },
      { icon: 'md-globe', ios: 'ios-globe', title: 'Noticias', component: NewsUserPage },
      { icon: 'md-play', ios: 'ios-play', title: 'Youtube', component: YoutubeUserPage },
      { icon: 'md-alarm', ios: 'ios-alarm', title: 'Tips', component: TipsUserPage },
      { icon: 'md-send', ios: 'ios-send', title: 'Mensajería', component: MessagesTabPage },
      { icon: 'md-apps', ios: 'ios-apps', title: 'Cobradores', component: CobradoresPage },
      { icon: 'md-color-palette', ios: 'ios-color-palette', title: 'Personalizar', component: PersonalizationUserPage },
      { icon: 'md-person', ios: 'ios-person', title: 'Configuración de la Cuenta', component: ProfileUserPage }
    ];
  }

  loadModulesZFIT() {
    this.pages = [
      { icon: 'md-apps', ios: 'ios-apps', title: 'Categorias', component: CategorysUserPage },
      { icon: 'md-calendar', ios: 'ios-calendar', title: 'Eventos', component: EventsUserPage },
      { icon: 'ios-basket', ios: 'ios-basket-outline', title: 'Productos', component: ProductsUserPage },
      { icon: 'md-book', ios: 'ios-book', title: 'Pedidos', component: OrdersUserPage },
      { icon: 'md-card', ios: 'ios-card', title: 'Membresías', component: MembershipUserPage },
      { icon: 'md-globe', ios: 'ios-globe', title: 'En Vivo', component: NewsUserPage },
      { icon: 'logo-rss', ios: 'logo-rss', title: 'Blog & Tutoriales', component: BlogUserPage },
      { icon: 'md-thumbs-up', ios: 'ios-thumbs-up', title: 'RSE - Social', component: SocialUserPage },
      { icon: 'md-send', ios: 'ios-send', title: 'Mensajería', component: MessagesTabPage },
      { icon: 'md-happy', ios: 'ios-happy', title: 'Mis Beneficios', component: BenefitsUserPage },
      { icon: 'md-walk', ios: 'ios-walk', title: 'Workouts', component: WorkoutsUserPage },
      { icon: 'logo-youtube', ios: 'logo-youtube', title: 'Youtube', component: YoutubeUserPage },
      { icon: 'md-nutrition', ios: 'ios-nutrition', title: 'Tips Nutricionales', component: TipsUserPage },
      { icon: 'md-play', ios: 'ios-play', title: 'Pausa Activa', component: ActivePauseUserPage },
      { icon: 'md-medal', ios: 'ios-medal', title: 'Leaderboard', component: LeadearboardUserPage },
      { icon: 'md-body', ios: 'ios-body', title: 'Progresos', component: ProgressUserPage },
      { icon: 'md-people', ios: 'ios-people', title: 'ZFIT Club', component: ZFitClubUserPage },
      { icon: 'md-stopwatch', ios: 'ios-stopwatch', title: 'Running Club', component: RunningClubUserPage },
      { icon: 'md-planet', ios: 'ios-planet', title: 'Staff', component: StaffUserPage },
      { icon: 'md-pricetag', ios: 'ios-pricetag', title: 'Mis Promociones', component: PromotionUserPage },
      { icon: 'md-leaf', ios: 'ios-leaf', title: 'Motivación', component: MotivationsUserPage },
      { icon: 'md-color-palette', ios: 'ios-color-palette', title: 'Personalizar', component: PersonalizationUserPage },
      { icon: 'md-person', ios: 'ios-person', title: 'Configuración de la Cuenta', component: ProfileUserPage }
    ];
  }

  public loadSingleUser() {
    if((this.baseId+'')!='null'){
      this.usersService.getSingle(this.baseId)
      .then(response => {
        this.baseUser = response;
        this.pictureCover = response.pic3;
        localStorage.setItem('currentPictureCover', response.pic3);
      }).catch(error => {
        console.clear();
      })
    }
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  openLeadearboard() {
    this.nav.setRoot(LeadearboardUserPage);
  }

  openBlog() {
    this.nav.setRoot(BlogUserPage);
  }

  openWorkouts() {
    this.nav.setRoot(WorkoutsUserPage);
  }

  openPromotions() {
    this.nav.setRoot(PromotionUserPage);
  }

  openAdoptions() {
    this.nav.setRoot(AdoptionUserPage);
  }

  openSocial() {
    this.nav.setRoot(SocialUserPage);
  }

  openActivePause() {
    this.nav.setRoot(ActivePauseUserPage);
  }

  openRunningClub() {
    this.nav.setRoot(RunningClubUserPage);
  }

  openBenefits() {
    this.nav.setRoot(BenefitsUserPage);
  }

  openProgress() {
    this.nav.setRoot(ProgressUserPage);
  }

  openZFit() {
    this.nav.setRoot(ZFitClubUserPage);
  }

  openStaff() {
    this.nav.setRoot(StaffUserPage);
  }

  openMotivation() {
    this.nav.setRoot(MotivationsUserPage);
  }

  openDemo() {
    this.nav.setRoot(DemoMapsPage);
  }

  logOut() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentEmail');
    localStorage.removeItem('currentId');
    localStorage.removeItem('currentState');
    localStorage.removeItem('currentRolId');
    localStorage.removeItem('currentPicture');
    localStorage.removeItem('currentFirstName');
    localStorage.removeItem('currentLastName');
    localStorage.removeItem('currentMessages');
    localStorage.clear();
    this.loading.create({
      content: "Cerrando Sesión...",
      duration: 1000
      }).present();
    //this.nav.setRoot(LoginPage);
    location.reload()

  }
}
