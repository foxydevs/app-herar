import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { OrdersUserPage } from '../orders-user/orders-user';
import { SeeProductsUserPage } from './see-products-user/see-products-user';
import { ProductsFormUserPage } from './form-products-user/form-products-user';
import { ProductsFormUpdateUserPage } from './form-products-update-user/form-products-update-user';

//JQUERY
declare var $:any;

@Component({
  selector: 'products-user',
  templateUrl: 'products-user.html'
})
export class ProductsUserPage {
  //PROPIEDADES
  private products:any[] = [];
  private parameter:any;
  private idUser:any;
  private search:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public productsService: ProductsService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams
  ) {
    this.parameter = this.navParams.get('parameter');
    this.idUser = localStorage.getItem("currentId");
    if(this.parameter) {
      this.loadAllForCategories(this.parameter);
    } else {
      this.loadAll();
    }
  }

  //CARGAR PRODUCTOS POR CATEGORIA
  public loadAllForCategories(id:any){
    this.productsService.getAllUser(this.idUser)
    .then(response => {
      this.products = [];
      for(let x of response) {
        if(x.category == id) {
          this.products.push(x);
        }
      }
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR PRODUCTOS
  public loadAll() {
    this.productsService.getAllUser(this.idUser)
    .then(response => {
      this.products = [];
      this.products = response;
    }).catch(error => {
      console.clear
    })
  }

  public seeOrders(parameter:any) {
    this.loading.create({
      content: "Cargando",
      duration: 750
    }).present();
    this.navCtrl.push(OrdersUserPage, { parameter });
  }

  public seeMore(parameter:any) {
    this.navCtrl.push(SeeProductsUserPage, { parameter });
  }

  //Ver Formulario Agregar
  public viewForm() {
    var parameter = this.parameter;
    if(this.parameter) {
      this.navCtrl.push(ProductsFormUserPage, { parameter });
    } else {
      this.navCtrl.push(ProductsFormUserPage);
    }
  }

  public viewUpdateForm(parameter:any) {
    this.navCtrl.push(ProductsFormUpdateUserPage, { parameter });
  }

  ionViewWillEnter() {
    if(this.parameter) {
      this.loadAllForCategories(this.parameter);
    } else {
      this.loadAll();
    }
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el producto?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando...",
              duration: 2000
            })
            load.present()
            this.productsService.delete(id)
            .then(res => {
              load.dismiss();
              this.loadAll();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.parameter) {
        this.loadAllForCategories(this.parameter);
      } else {
        this.loadAll();
      }
      refresher.complete();
    }, 2000);
  }

}
