import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { path } from "../../../../app/config.module";
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

//JQUERY
declare var $:any;

@Component({
  selector: 'form-products-update-user',
  templateUrl: 'form-products-update-user.html'
})
export class ProductsFormUpdateUserPage implements OnInit {
  //Propiedades
  private categories:any[] = [];
  private product = {
    name: '',
    description : '',
    price: 0.00,
    quantity: 0,
    cost : 0.00,
    category: '',
    picture: '',
    tiempo: 0,
    periodo: 0,
    membresia: 0,
    nivel: 1,
    picture1: localStorage.getItem('currentPicture'),
    picture2: localStorage.getItem('currentPicture'),
    picture3: localStorage.getItem('currentPicture'),
    idPic1: '',
    idPic2: '',
    idPic3: '',
    id: ''
  }
  private parameter:any;
  private basePath:string = path.path
  idUser = localStorage.getItem("currentId");
  private btnDisabled:boolean = false;
  private images:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //Constructor
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public alertCtrl: AlertController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.loadAllCategories();
    this.getProduct();
    this.getImage();
  }

  ngOnInit() {
  }

  //CARGAR LAS CATEGORIAS
  public loadAllCategories(){
    this.categorysService.getAllUser(this.idUser)
    .then(response => {
      this.categories = response;
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR PRODUCTO
  public getProduct() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.productsService.getSingle(this.parameter)
    .then(response => {
      this.product.id = response.id;
      this.product.name = response.name;
      this.product.description = response.description;
      this.product.price = response.price.toFixed(2);
      this.product.quantity = response.quantity;
      this.product.cost = response.cost.toFixed(2);
      this.product.category = response.category;
      this.product.picture = response.picture;
      this.product.tiempo = response.tiempo;
      this.product.periodo = response.periodo;
      this.product.membresia = response.membresia;
      this.product.nivel = response.nivel;
      if(response.pictures[0]) {
        this.product.picture1 = response.pictures[0].picture;
        this.product.idPic1 = response.pictures[0].id;
      } 
      if(response.pictures[1]) {
        this.product.picture2 = response.pictures[1].picture;
        this.product.idPic2 = response.pictures[1].id;
      }
      if(response.pictures[2]) {
        this.product.picture3 = response.pictures[2].picture;
        this.product.idPic3 = response.pictures[2].id;
      }
      load.dismiss();
    }).catch(error => {
      console.log(error)
    })
  }

  //CARGAR PRODUCTO
  public getInformationImage() {
    this.productsService.getSingle(this.parameter)
    .then(response => {
      if(response.pictures[0]) {
        this.product.picture1 = response.pictures[0].picture;
        this.product.idPic1 = response.pictures[0].id;
      } 
      if(response.pictures[1]) {
        this.product.picture2 = response.pictures[1].picture;
        this.product.idPic2 = response.pictures[1].id;
      }
      if(response.pictures[2]) {
        this.product.picture3 = response.pictures[2].picture;
        this.product.idPic3 = response.pictures[2].id;
      }
    }).catch(error => {
      console.log(error)
    })
  }

  public getImage() {
    for(let i=0; i<3; i++) {
      let image = {
        avatar: ''
      }
      this.images.push(image)
    }
  }

  //ACTUALIZAR
  public update() {
    if(this.product.name) {
      if(this.product.description) {
        if(this.product.price >= 0) {
          if(this.product.quantity >= 0) {
            if(this.product.cost  >= 0) {
              if(this.product.category) {
                this.btnDisabled = true;
                this.loading.create({
                  content: "Actualizando Producto...",
                  duration: 1500
                }).present();
                this.productsService.update(this.product)                
                .then(response => {                  
                  this.confirmation('Producto Actualizado', 'El producto fue actualizado exitosamente.');
                  this.navCtrl.pop();
                  console.clear();
                  }).catch(error => {
                  this.btnDisabled = false;
                  console.clear();
                });
              } else {
                this.message('La categoría es requerida.')
              }
            } else {
              this.message('El costo es requerido.')
            }
          } else {
            this.message('La cantidad es requerida.')
          }
        } else {
          this.message('El precio es requerido.')
        }
      } else {
        this.message('La descripción es requerida.')
      }
    } else {
      this.message('El nombre es requerido.')
    }    
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  public confirmation(title: any, message?:any) {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //SUBIR IMAGEN PRINCIPAL
  uploadImagePrincipal(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}product/upload/${this.product.id}`;
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {        
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif');          
        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.picture);
            $("#"+id).val('');  
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //SUBIR IMAGEN PRINCIPAL
  uploadImages(archivo, id, img, idPicture) {
    this.getInformationImage();
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}product/upload/multiple/${this.product.id}`;
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {        
      if(size<(2*(1024*1024))) {
        $('#'+img).attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif');          
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            user: this.idUser,
            idPic: idPicture
          },
          function(respuesta) {
            console.log(respuesta)
            $('#'+img).attr("src", respuesta.picture);
            $("#"+id).val('');  
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

}
