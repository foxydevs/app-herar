import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
//import { ProductsUserPage } from './../products-user';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { path } from '../../../../app/config.module';

//JQUERY
declare var $:any;

@Component({
  selector: 'form-products-user',
  templateUrl: 'form-products-user.html'
})
export class ProductsFormUserPage implements OnInit {
  private users:any[] = [];
  private categories:any[] = [];
  private product = {
    name: '',
    id: '',
    description : '',
    price: 0,
    quantity: 0,
    tiempo: 0,
    periodo: 0,
    membresia: 0,
    nivel: 1,
    cost : 0,
    user_created: '',
    category: '',
    picture: localStorage.getItem('currentPicture')
  }
  private parameter:any;
  private basePath:string = path.path
  idUser = localStorage.getItem("currentId");
  private btnDisabled:boolean = false;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public alertCtrl: AlertController
  ) {
    this.parameter = this.navParams.get('parameter');
    if(this.parameter) {
      this.product.category = this.parameter;
    }
    this.product.user_created = localStorage.getItem("currentId");
    this.loadAllCategories();
    this.loadAllUsers();
  }

  ngOnInit() {
  }

  //Cargar los productos
  public loadAllCategories(){
      this.categorysService.getAllUser(this.idUser)
      .then(response => {
          this.categories = response;
      }).catch(error => {
            console.clear
      })
  }

  //Cargar los productos
  public loadAllUsers(){
      this.usersService.getAll()
      .then(response => {
          this.users = response;
      }).catch(error => {
            console.clear
      })
  }

  //INGRESO DE DATOS
  public insert() {
    //var parameter = this.parameter;
    if(this.product.name) {
      if(this.product.description) {
        if(this.product.price >= 0) {
          if(this.product.quantity >= 0) {
            if(this.product.cost >= 0) {
              if(this.product.category) {
                this.btnDisabled = true;
                this.loading.create({
                  content: "Cargando...",
                  duration: 1500
                }).present();
                this.productsService.create(this.product)
                .then(response => { 
                  this.confirmation('Producto Agregado!', 'El producto fue agregado exitosamente.');
                  this.product.id = response.id;
                  console.log(this.product.id)                  
                    //if(this.parameter) {
                      //this.navCtrl.setRoot(ProductsUserPage, { parameter });
                    //} else {
                      //this.navCtrl.setRoot(ProductsUserPage);
                    ///}
                    }).catch(error => {
                    this.btnDisabled = false;
                    //console.clear();
                });
              } else {
                this.message('La categoría es requerida.')
              }
            } else {
              this.message('El costo es requerido.')
            }
          } else {
            this.message('La cantidad es requerida.')
          }
        } else {
          this.message('El precio es requerido.')
        }
      } else {
        this.message('La descripción es requerida.')
      }
    } else {
      this.message('El nombre es requerido.')
    }    
  }

  //SUBIR IMAGENES
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}product/upload/${this.product.id}`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            //$('#imgAvatar').attr("src",'')
            $('#imgAvatar').attr("src", respuesta.picture)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  public confirmation(title: any, message?:any) {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

}
