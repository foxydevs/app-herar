import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';
import { FormEventsUserPage } from './form-events-user/form-events-user';

 @Component({
   selector: 'events-user',
   templateUrl: 'events-user.html'
 })
 export class EventsUserPage {
  //PROPIEDADES
  private events:any[] = [];
  private idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
     public loading: LoadingController,
     public eventsService: EventsService,
     public alertCtrl :AlertController
  ) {
    this.idUser = localStorage.getItem("currentId");
  }

  //GET
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllType(this.idUser, 1).then(response => {
      this.events = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //ABRIR FORMULARIO
  openForm(parameter?:any) {
    this.navCtrl.push(FormEventsUserPage, { parameter })
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //ENTER
  ionViewWillEnter(){
    this.getAll();
  }

}
