import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PromotionService } from '../../../app/service/promotion.service';
import { FormPromotionUserPage } from './form-promotion-user/form-promotion-user';

@Component({
  selector: 'promotion-user',
  templateUrl: 'promotion-user.html',
})
export class PromotionUserPage {
  //PROPIEDADES
  private table:any[] = [];
  private idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  selectItem:any = 'beneficios';

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: PromotionService,
  ) {
    this.idUser = localStorage.getItem('currentId');
  }

  openForm(parameter?:any) {
    this.navCtrl.push(FormPromotionUserPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

}
