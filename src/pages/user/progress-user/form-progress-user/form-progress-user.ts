import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { ProgressService } from '../../../../app/service/progress.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'form-progress-user',
  templateUrl: 'form-progress-user.html',
})
export class FormProgressUserPage {
  //PROPIEDADES
  private parameter:any;
  private title:any;
  private disabledBtn:boolean;
  private basePath:string = path.path;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  private data = {
    client: '',
    user: path.id,
    tipo: '1',
    state: '1',
    foto_espalda: localStorage.getItem('currentPicture'),
    foto_frente: localStorage.getItem('currentPicture'),
    comentario: '',
    complexion: '',
    imc: '',
    opcion1: '',
    opcion2: localStorage.getItem('currentPicture'),
    peso: '',
    grasa: '',
    masa_muscular: '',
    imc_num: '',
    opcionNum1: '',
    opcionNum2: '',
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: ProgressService
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    if(this.parameter.idClient) {
      this.title = "Agregar Progreso"; 
    } else {
      this.title = "Edición Progreso";
      this.getSingle(this.parameter);  
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    this.data.foto_espalda = $('img[alt="Avatar2"]').attr('src');
    this.data.foto_frente = $('img[alt="Avatar"]').attr('src');
    this.data.opcion2 = $('img[alt="Avatar3"]').attr('src');
    if(this.parameter.idClient) {
      this.title = "Agregar Progreso";
      this.data.client = this.parameter.idClient;
      console.log(this.data)
      this.create(this.data)
    } else {
      this.title = "Edición Progreso"; 
      this.update(this.data)
    }
  }

  //AGREGAR
  create(formValue:any) {
    let load = this.loading.create({
      content: "Eliminando..."
    });
    load.present();
    this.mainService.create(formValue)
    .then(response => {
      load.dismiss();
      this.confirmation('Progreso Agregado', 'El progreso fue agregado exitosamente.');
      this.navCtrl.pop();
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    let load = this.loading.create({
      content: "Eliminando..."
    });
    load.present();
    this.mainService.update(formValue)
    .then(response => {
      load.dismiss();
      this.confirmation('Progreso Actualizado', 'El progreso fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el workout?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'progress'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //IMAGEN DE CATEGORIA
  uploadImage2(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar2').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'progress'
          },
          function(respuesta) {
            $('#imgAvatar2').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //IMAGEN DE CATEGORIA
  uploadImage3(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar3').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'progress'
          },
          function(respuesta) {
            $('#imgAvatar3').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }
}
