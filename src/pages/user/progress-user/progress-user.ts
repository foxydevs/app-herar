import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { FormProgressUserPage } from './form-progress-user/form-progress-user';
import { ProgressService } from '../../../app/service/progress.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'progress-user',
  templateUrl: 'progress-user.html'
})
export class ProgressUserPage {
  //PROPIEDADES
  private idUser:any;
  private search:any;
  private search2:any;
  private users:any[] = [];
  private table:any[] = [];
  selectItem:any = 'progresos';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: UsersService,
    public alertCtrl: AlertController,
    public secondService: ProgressService,
  ) {
    this.idUser = localStorage.getItem('currentId')
  }

  //CARGAR USUARIOS
  public getAll(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.mainService.getClients(this.idUser)
    .then(response => {
      this.users = [];
      this.users = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.secondService.getAllUser(this.idUser)
    .then(response => {
      this.table = [];
      this.table = response;
      this.table.reverse();
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList button").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //BUSCAR
  public searchTable2() {
    var value = this.search2.toLowerCase();
    $("#myList ion-item").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //OPEN FORM
  openForm(idClient:any) {
    let parameter = {
      idClient: idClient
    }
    this.navCtrl.push(FormProgressUserPage, { parameter })
  }

  //OPEN FORM
  openFormUpdate(parameter:any) {
    this.navCtrl.push(FormProgressUserPage, { parameter })
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el progreso del usuario?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.secondService.delete(id)
            .then(response => {
              this.getAllSecond()
              console.clear();
            }).catch(error => {
              console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    if(this.selectItem == 'progresos') {
      this.getAllSecond();        
    } else {
      this.getAll()
    }
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.selectItem == 'progresos') {
        this.getAllSecond();        
      } else {
        this.getAll()
      }
      refresher.complete();
    }, 2000);
  }
}