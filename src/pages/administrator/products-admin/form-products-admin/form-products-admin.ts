import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsAdminPage } from './../products-admin';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';

@Component({
  selector: 'form-products-admin',
  templateUrl: 'form-products-admin.html'
})
export class ProductsFormAdminPage implements OnInit {
  private users:any[] = [];
  private categories:any[] = [];
  private product = {
    name: '',
	description : '',
	price: '',
	quantity: '',
	cost : '',
	user_created: '',
	category: ''
  }
  private title:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController
  ) {

    this.loadAllCategories();
    this.loadAllUsers();
    this.title = "Nuevo Producto";
  }

  ngOnInit() {
  }

  //Cargar los productos
  public loadAllCategories(){
      this.categorysService.getAll()
      .then(response => {
          this.categories = response;
      }).catch(error => {
            console.clear
      })
  }

  //Cargar los productos
  public loadAllUsers(){
      this.usersService.getAll()
      .then(response => {
          this.users = response;
      }).catch(error => {
            console.clear
      })
  }

  //Insertar Datos
  insert(){
    this.productsService.create(this.product)
    .then(response => {
        //this.cargarAll()
        let loader = this.loading.create({
        content: "Registrando Producto...",
        duration: 2000
        });
        loader.present();
        this.navCtrl.setRoot(ProductsAdminPage);
        console.clear
        }).catch(error => {
            console.clear
    })
  }

}