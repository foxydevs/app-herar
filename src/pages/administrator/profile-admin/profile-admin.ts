import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChangePasswordProfileAdminPage } from './change-password-admin/change-password-admin';
import { ProfileUpdateAdminPage } from './profile-update-admin/profile-update-admin';
import { UsersTypesService } from '../../../app/service/users-types.service';

@Component({
  selector: 'profile-admin',
  templateUrl: 'profile-admin.html'
})
export class ProfileAdminPage {
  //Propiedades
  private profilePicture:any;
  private idUser:any;
  
  constructor(
    public navCtrl: NavController,
    public usersTypesService: UsersTypesService
  ) {
      this.idUser = localStorage.getItem('currentId');
    this.usersTypesService.getSingle(this.idUser)
    .then(response => {
      this.profilePicture = response.picture;
    }).catch(error => {
      console.clear();
    })
  }

  //Cambiar Contraseña
  public changePassword() {
    this.navCtrl.push(ChangePasswordProfileAdminPage);
  }

  //Actualizar Usuario
  public updateUser() {
    this.navCtrl.push(ProfileUpdateAdminPage);
  }

}
