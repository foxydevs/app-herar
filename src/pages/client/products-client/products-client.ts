import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { OrdersClientPage } from '../orders-client/orders-client';
import { SeeProductsClientPage } from './see-products-client/see-products-client';
import { path } from "./../../../app/config.module";
import { MembershipService } from '../../../app/service/membership.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'products-client',
  templateUrl: 'products-client.html'
})
export class ProductsClientPage {
  //Propiedades
  private products:any[] = [];
  private parameter:any;
  private baseId:number = path.id;
  private search:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //Constructor
  constructor(
    public navCtrl: NavController,
    public productsService: ProductsService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams,
    public mainService: MembershipService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.mainService.calculateMembership();
    if(this.parameter) {
      setTimeout(() => {
        this.loadAll(this.parameter);
      }, 500);
    } else {
      this.loadAllProducts()
    }
  }

  //Cargar los Productos
  public loadAll(id:any){
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    if((this.baseId+'')!='null'){
      this.productsService.getAllUser(this.baseId)
      .then(response => {
        for(let x of response) {
          if(x.category == id) {
            this.products.push(x);
          }
        }
        load.dismiss();
      }).catch(error => {
        console.clear;
      })
    }else{
      this.productsService.getAll()
      .then(response => {
        for(let x of response) {
          if(x.category == id) {
            this.products.push(x);
          }
        }
        load.dismiss();
      }).catch(error => {
        console.clear;
      })
    }
  }

  public loadAllProducts(){
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.productsService.getAllUser(this.baseId)
    .then(response => {
      load.dismiss();
      this.products = response;
    }).catch(error => {
      console.clear;
    })
  }

  public makeAnOrder(parameter:any) {
    this.navCtrl.push(OrdersClientPage, { parameter });
  }

  public seeMore(parameter:any) {
    this.navCtrl.push(SeeProductsClientPage, { parameter });
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.parameter) {
        setTimeout(() => {
          this.loadAll(this.parameter);
        }, 500);
      } else {
        this.loadAllProducts()
      }
      refresher.complete();
    }, 2000);
  }

}
