import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { SeeProductsClientCommentPage } from './see-products-client-comment';

//import { path } from "./../../../../app/config.module";

@Component({
  selector: 'see-products-client',
  templateUrl: 'see-products-client.html'
})
export class SeeProductsClientPage implements OnInit {
  private users:any[] = [];
  private comments:any[] = [];
  private product = {
    name: '',
    description : '',
    price: '',
    quantity: '',
    cost : '',
    user_created: '',
    category: '',
    created_at: '',
    update_at: '',
    picture: '',
    picture1: '',
    picture2: '',
    picture3: '',
    id: ''
  }
  //private baseId:number = path.id;
  private parameter:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  moneda = localStorage.getItem('currentCurrency');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public modalCtrl: ModalController
  ) {
    this.loadAllUsers();
    this.parameter = this.navParams.get('parameter');
  }

  ngOnInit() {
  }

  //Cargar los productos
  public loadAllUsers(){
      this.usersService.getAll()
      .then(response => {
          this.users = response;
      }).catch(error => {
            console.clear;
      })
  }

  public loadProduct(idProduct:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.productsService.getSingle(idProduct)
    .then(response => {
      try{
        this.product.name = response.name;
        this.product.description = response.description;
        this.product.price = parseFloat(response.price).toFixed(2).toString();
        this.product.quantity = response.quantity;
        this.product.user_created = this.returnNameUser(response.user_created);
        this.product.created_at = response.created_at;
        this.product.update_at = response.updated_at;
        this.product.picture = response.picture;
        this.product.id = response.id;
      }catch(error){
        console.log(error);

      }
      // console.log(response);
      if(response.pictures){
        if(response.pictures[0]) {
          this.product.picture1 = response.pictures[0].picture;
        }
        if(response.pictures[1]) {
          this.product.picture2 = response.pictures[1].picture;
        }
        if(response.pictures[2]) {
          this.product.picture3 = response.pictures[2].picture;
        }
      }
      load.dismiss();
    }).catch(error => {
      console.clear;
    })
  }

  public openPage(parameter:any) {
    this.navCtrl.push(SeeProductsClientCommentPage, { parameter });
  }

  //CARGAR COMENTARIOS POR PRODUCTOS
  public loadCommentsByProducts(id:any) {
    this.comments = [];
    this.productsService.getAllCommentsByProduct(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: x.created_at,
          user: this.returnNameUser(x.user),
          picture: this.returnPicture(x.user)
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //Devolver el Nombre del Usuario
   public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.loadProduct(this.parameter);
      this.loadCommentsByProducts(this.parameter);
    }, 1000);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadCommentsByProducts(this.parameter);
      refresher.complete();
    }, 2000);
  }

}
