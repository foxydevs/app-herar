import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategorysClientPage } from './categorys-client/categorys-client';
import { MyOrdersClientPage } from './orders-client/my-orders/my-orders';
import { ProfileClientPage } from './profile-client/profile-client';
import { EventsClientPage } from './events-client/events-client';
import { MessagesClientPage } from './messages-client/messages-client';
import { UsersService } from '../../app/service/users.service';
import { ProductsClientPage } from './products-client/products-client';
import { Events } from 'ionic-angular/util/events';
import { NewsClientPage } from './news-client/news-client';
import { TipsClientPage } from './tips-client/tips-client';
import { YoutubeClientPage } from './youtube-client/youtube-client';
import { AccessService } from '../../app/service/access.service';
import { path } from '../../app/config.module';
import { LeadearboardClientPage } from './leadearboard-client/leadearboard-client';
import { BlogClientPage } from './blog-client/blog-client';
import { WorkoutsClientPage } from './workouts-client/workouts-client';
import { AdoptionClientPage } from './adoption-client/adoption-client';
import { SocialClientPage } from './social-client/social-client';
import { ActivePauseClientPage } from './active-pause-client/active-pause-client';
import { PromotionClientPage } from './promotion-client/promotion-client';
import { RunningClubClientPage } from './running-club-client/running-club-client';
import { MyMembershipClientPage } from './my-membership-client/my-membership-client';
import { BenefitsClientPage } from './benefits-client/benefits-client';
import { MyProgressClientPage } from './my-progress-client/my-progress-client';
import { ZFitClubClientPage } from './zfit-club-client/zfit-club-client';
import { StaffClientPage } from './staff-client/staff-client';
import { MotivationClientPage } from './motivation-client/motivation-client';

@Component({
  selector: 'page-client',
  templateUrl: 'client.html'
})
export class ClientPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  pages: Array<{icon:string, ios:string, title: string, apodo?: string, component: any, membresia: any}>;
  private picture:any;
  private email:any;
  private firstName:any;
  private baseUser:any;
  private baseId:any;
  private baseIdUser:number = path.id;
  private pictureCover:any;
  private opcion2:any;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loading: LoadingController,
    public events: Events,
    private usersService: UsersService,
    private accessService: AccessService) {
    //CARGAR USUARIO    
    this.getData();
    this.getPages();
    this.loadModules();
    if(localStorage.getItem('currentpictureCover')!='null'){
      this.pictureCover = localStorage.getItem('currentpictureCover');
    }
    events.subscribe('user:update', res => {
      this.loadModules();
      this.picture = localStorage.getItem("currentPicture");
      this.email = localStorage.getItem("currentEmail");
      this.firstName = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    });
  }

  public loadSingleUser(id:any){
    this.usersService.getSingle(id)
    .then(response => {
      this.baseUser = response;
      this.picture = response.picture;
      localStorage.setItem("currentPicture",response.picture);
      //console.clear();
    }).catch(error => {
      //console.clear();
    })
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    localStorage.setItem('currentNivelMembresia', page.membresia);
    this.nav.setRoot(page.component);
  }

  logOut() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentEmail');
    localStorage.removeItem('currentId');
    localStorage.removeItem('currentState');
    localStorage.removeItem('currentRolId');
    localStorage.removeItem('currentPicture');
    localStorage.removeItem('currentFirstName');
    localStorage.removeItem('currentLastName');
    localStorage.removeItem('currentMessages');
    localStorage.clear();
    this.loading.create({
      content: "Cerrando Sesión...",
      duration: 500
    }).present();
    location.reload();
  }
  openAdoptions() {
    this.nav.setRoot(AdoptionClientPage);
  }

  public loadModules(){
    this.accessService.getAccess(this.baseIdUser)
    .then(response => {      
      if(response.permitidos.length > 0){
        this.pages = [];
        for(let x of response.permitidos) {
          if(x.modulos_show.id == '16' && x.mostrar == '1') {
            this.pages.push({icon: 'md-star', ios: 'ios-star', title: x.modulos_show.nombre, apodo: x.apodo, component: MyMembershipClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '1' && x.mostrar == '1') {
            this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: CategorysClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '2' && x.mostrar == '1') {
            this.pages.push({icon: 'md-albums', ios: 'ios-albums', title: x.modulos_show.nombre, apodo: x.apodo, component: ProductsClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '3' && x.mostrar == '1') {
            this.pages.push({icon: 'md-cart', ios: 'ios-cart', title: x.modulos_show.nombre, apodo: x.apodo, component: MyOrdersClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '4' && x.mostrar == '1') {
            this.pages.push({icon: 'md-calendar', ios: 'ios-calendar', title: x.modulos_show.nombre, apodo: x.apodo, component: EventsClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '5' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-youtube', ios: 'logo-youtube', title: x.modulos_show.nombre, apodo: x.apodo, component: YoutubeClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '6' && x.mostrar == '1') {
            this.pages.push({icon: 'md-globe', ios: 'ios-globe', title: x.modulos_show.nombre, apodo: x.apodo, component: NewsClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '7' && x.mostrar == '1') {
            if(this.baseIdUser == 49) {
              this.pages.push({icon: 'md-nutrition', ios: 'ios-nutrition', title: x.modulos_show.nombre, apodo: x.apodo, component: TipsClientPage, membresia: x.membresia})
            } else {
              this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: TipsClientPage, membresia: x.membresia})
            }
          } else if(x.modulos_show.id == '8' && x.mostrar == '1') {
            this.pages.push({icon: 'md-send', ios: 'ios-send', title: x.modulos_show.nombre, apodo: x.apodo, component: MessagesClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '9' && x.mostrar == '1') {
            this.pages.push({icon: 'md-person', ios: 'ios-person', title: x.modulos_show.nombre, apodo: x.apodo, component: ProfileClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '10' && x.mostrar == '1') {
            this.pages.push({icon: 'md-walk', ios: 'ios-walk', title: x.modulos_show.nombre, apodo: x.apodo, component: WorkoutsClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '11' && x.mostrar == '1') {
            this.pages.push({icon: 'md-medal', ios: 'ios-medal', title: x.modulos_show.nombre, apodo: x.apodo, component: LeadearboardClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '12' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-rss', ios: 'logo-rss', title: x.modulos_show.nombre, apodo: x.apodo, component: BlogClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '13' && x.mostrar == '1') {
            this.pages.push({icon: 'md-thumbs-up', ios: 'ios-thumbs-up', title: x.modulos_show.nombre, apodo: x.apodo, component: SocialClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '14' && x.mostrar == '1') {
            this.pages.push({icon: 'md-play', ios: 'ios-play', title: x.modulos_show.nombre, apodo: x.apodo, component: ActivePauseClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '15' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetag', ios: 'ios-pricetag', title: x.modulos_show.nombre, apodo: x.apodo, component: PromotionClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '17' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetags', ios: 'ios-pricetags', title: x.modulos_show.nombre, apodo: x.apodo, component: BenefitsClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '18' && x.mostrar == '1') {
            this.pages.push({icon: 'md-body', ios: 'ios-body', title: x.modulos_show.nombre, apodo: x.apodo, component: MyProgressClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '19' && x.mostrar == '1') {
            this.pages.push({icon: 'md-stopwatch', ios: 'ios-stopwatch', title: x.modulos_show.nombre, apodo: x.apodo, component: RunningClubClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '20' && x.mostrar == '1') {
            this.pages.push({icon: 'md-people', ios: 'ios-people', title: x.modulos_show.nombre, apodo: x.apodo, component: ZFitClubClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '21' && x.mostrar == '1') {
            this.pages.push({icon: 'md-planet', ios: 'ios-planet', title: x.modulos_show.nombre, apodo: x.apodo, component: StaffClientPage, membresia: x.membresia})
          } else if(x.modulos_show.id == '22' && x.mostrar == '1') {
            this.pages.push({icon: 'md-leaf', ios: 'ios-leaf', title: x.modulos_show.nombre, apodo: x.apodo, component: MotivationClientPage, membresia: x.membresia})
          }
        }
      }
      console.clear
    }).catch(error => {
      console.log(error);
    })
  }
  
  public getPages() {
    if(this.opcion2 == '1') {
      this.rootPage = CategorysClientPage;
    } else if(this.opcion2 == '2') {
      this.rootPage = ProductsClientPage;
    }else if(this.opcion2 == '3') {
      this.rootPage = MyOrdersClientPage;      
    }else if(this.opcion2 == '4') {
      this.rootPage = EventsClientPage;      
    }else if(this.opcion2 == '5') {
      this.rootPage = YoutubeClientPage;      
    }else if(this.opcion2 == '6') {
      this.rootPage = NewsClientPage;      
    }else if(this.opcion2 == '7') {
      this.rootPage = TipsClientPage;
    } else {
      this.rootPage = CategorysClientPage;      
    }
  }

  /*public loadStatic() {
    this.pages = [
      { icon: 'md-apps', ios: 'ios-apps', title: 'Categorias', component: CategorysClientPage },
      { icon: 'md-basket', ios: 'ios-basket', title: 'Productos', component: ProductsClientPage },
      { icon: 'md-book', ios: 'ios-book', title: 'Pedidos', component: MyOrdersClientPage },
      { icon: 'md-calendar', ios: 'ios-calendar', title: 'Eventos', component: EventsClientPage },
      { icon: 'md-globe', ios: 'ios-globe', title: 'Noticias', component: NewsClientPage },
      { icon: 'md-play', ios: 'ios-play', title: 'Youtube', component: YoutubeClientPage },
      { icon: 'md-alarm', ios: 'ios-alarm', title: 'Tips', component: TipsClientPage },
      { icon: 'md-send', ios: 'ios-send', title: 'Mensajería', component: MessagesClientPage },
      { icon: 'md-person', ios: 'ios-person', title: 'Configuración de la Cuenta', component: ProfileClientPage },
      { icon: 'md-walk', ios: 'ios-walk', title: 'Workouts', component: WorkoutsClientPage },
      { icon: 'md-medal', ios: 'ios-medal', title: 'Leaderboard', component: LeadearboardClientPage },
      { icon: 'md-play', ios: 'ios-play', title: 'Pausa Activa', component: ActivePauseClientPage },
      { icon: 'md-thumbs-up', ios: 'ios-thumbs-up', title: 'Social', component: SocialClientPage },
      { icon: 'logo-rss', ios: 'logo-rss', title: 'Blog', component: BlogClientPage },
    ];
  }*/

  //GET DATOS CLIENTE
  public getData() {
    this.baseId = localStorage.getItem('currentId');
    this.picture = localStorage.getItem("currentPicture");
    this.email = localStorage.getItem("currentEmail");
    this.opcion2 = localStorage.getItem("currentHome");
    this.firstName = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.loadSingleUser(this.baseId);
  }

}
