import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { DetailSocialClientPage } from './detail-social-client/detail-social-client';
import { SocialService } from '../../../app/service/social.service';
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'social-client',
  templateUrl: 'social-client.html',
})
export class SocialClientPage {
  //PROPIEDADES
  private table:any[] = [];
  private idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: SocialService,
    public secondService: MembershipService
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.secondService.calculateMembership();
  }

  openForm(parameter?:any) {
    this.navCtrl.push(DetailSocialClientPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

}
