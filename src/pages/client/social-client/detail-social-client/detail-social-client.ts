import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { UsersService } from '../../../../app/service/users.service';
import { SocialService } from '../../../../app/service/social.service';
import { CommentSocialClientPage } from '../comment-social-client/comment-social-client';
import { ModalController } from '../../../../../node_modules/ionic-angular/components/modal/modal-controller';

@Component({
  selector: 'detail-social-client',
  templateUrl: 'detail-social-client.html',
})
export class DetailSocialClientPage {
  //PROPIEDADES
  private parameter:any;
  private users:any[] = [];
  private comments:any[] = [];
  private data:any[] = [];
  private id:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public modal: ModalController,
    public mainService: SocialService,
    public secondService: UsersService,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.id = this.parameter;
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR COMENTARIOS POR PRODUCTOS
  public getAllComments(id:any) {
    this.comments = [];
    this.mainService.getCommentsBySocial(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: x.created_at,
          user: this.returnNameUser(x.user),
          picture: this.returnPicture(x.user)
        }
        this.comments.push(comment);
        console.log(x)
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //Devolver el Nombre del Usuario
   public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }
  
  //ABRIR MODAL
  public openModal(parameter:any) {
    let chooseModal = this.modal.create(CommentSocialClientPage, { parameter });
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        this.getAllComments(this.id);
      }      
    });
   chooseModal.present();
  }

  ionViewWillEnter() {
    this.getSingle(this.parameter);
    this.getAllSecond();
    setTimeout(() => {
      this.getAllComments(this.id);
    }, 1500);
  }
}
