import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { EventsService } from '../../../../app/service/events.service';
import { SeeEventsClientPage } from './see-events-client';
import { ViewController } from 'ionic-angular/navigation/view-controller';

@Component({
  selector: 'see-events-client-comment',
  templateUrl: 'see-events-client-comment.html'
})
export class SeeEventsClientCommentPage{
  //PROPIEDADES
  private comment = {
    comment: '',
    event : '',
    user: ''
  }
  private parameter:any;
  private btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public eventsService: EventsService,
    public loading: LoadingController,
    public viewCtrl: ViewController
  ) {
    this.btnDisabled = false;
    this.parameter = this.navParams.get('parameter');
    this.comment.event = this.parameter;
    this.comment.user = localStorage.getItem('currentId');
  }

   //Insertar Datos
  insert(){
    //let id = this.comment.event;
    if(this.comment.comment) {
      this.btnDisabled = true;
      this.eventsService.createComment(this.comment)
      .then(response => {        
        this.loading.create({
          content: "Registrando Comentario...",
          duration: 2000
        }).present();
        this.viewCtrl.dismiss();
        console.clear();
      }).catch(error => {
        this.btnDisabled = false;
        console.clear();
      })
    } else {
      this.toast.create({
        message: "Ingrese un comentario.",
        duration: 800
      }).present();
    }
  }

  public returnEvents(parameter:any) {
    this.navCtrl.push(SeeEventsClientPage, { parameter });
  }
  

}