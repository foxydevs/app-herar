import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';
import { SeeEventsClientPage } from './see-events-client/see-events-client';

import { path } from "./../../../app/config.module";
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'events-client',
  templateUrl: 'events-client.html'
})
export class EventsClientPage {
  //PROPIEDADES
  private events:any[] = [];
  private baseId:number = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public eventsService: EventsService,
    public alertCtrl :AlertController,
    public mainService: MembershipService
  ) {
    this.getAll();
    this.mainService.calculateMembership();
  }
  
  //GET
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllType(this.baseId, 1).then(response => {
      this.events = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //Ver Más del Evento
  public viewMore(parameter: any) {
    this.loading.create({
      content: "Cargando",
      duration: 1000
    }).present();
    this.navCtrl.push(SeeEventsClientPage, { parameter });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
