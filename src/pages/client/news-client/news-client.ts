import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { NewsService } from '../../../app/service/news.service';
import { SeeNewsClientPage } from './see-news-client/see-news-client';
import { MembershipService } from '../../../app/service/membership.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'news-client',
  templateUrl: 'news-client.html'
})
export class NewsClientPage {
  //PROPIEDADES
  private news:any[] = [];
  private search:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public newsService: NewsService,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public mainService: MembershipService
  ) {
    this.mainService.calculateMembership();
  }

  //CARGAR PRODUCTOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.newsService.getAll()
    .then(response => {
      this.news = [];
      this.news = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  seeLiveVideo(parameter:any) {
    this.loading.create({
      content: 'Cargando...',
      duration: 1000
    }).present();
    this.navCtrl.push(SeeNewsClientPage, { parameter })   
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
