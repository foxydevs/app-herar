import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AdoptionDogsService } from '../../../../app/service/adoption-dogs.service';
import { FormAdoptionDogClientPage } from '../form-adoption-dog-client/form-adoption-dog-client';

@Component({
  selector: 'adoption-dogs-client',
  templateUrl: 'adoption-dogs-client.html',
})
export class AdoptionDogsClientPage {
  //PROPIEDADES
  private idUser:any;
  selectItem:any = 'proceso';
  private adoptionsDogs:any[] = [];
  private adoptionsDogs1:any[] = [];
  private adoptionsDogs2:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public modal: ModalController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: AdoptionDogsService,
  ) {
    this.idUser = localStorage.getItem('currentId')
  }

  openForm(parameter?:any) {
    this.navCtrl.push(FormAdoptionDogClientPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllClients(id)
    .then(response => {
      this.adoptionsDogs = []
      this.adoptionsDogs1 = []
      this.adoptionsDogs2 = []
      this.adoptionsDogs = response;
      this.adoptionsDogs1 = response;
      this.adoptionsDogs2 = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //ENVIAR FORMULARIO
  public sendEmailDog(id:string){
    this.mainService.sendEmail(id)
    .then(response => {
      console.log(response);
      this.confirmation('Revisa tu correo!', 'Te hemos enviado el formulario al correo: ');
    }).catch(error => {
      console.clear
    })
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la encuesta?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.getAll(this.idUser)
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

}
