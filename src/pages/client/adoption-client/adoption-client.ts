import { Component } from '@angular/core';
import { AdoptionDogsClientPage } from './adoption-dogs-client/adoption-dogs-client';
import { AdoptionCatsClientPage } from './adoption-cats-client/adoption-cats-client';

@Component({
  templateUrl: 'adoption-client.html'
})
export class AdoptionClientPage {

  tab1Root = AdoptionDogsClientPage;
  tab2Root = AdoptionCatsClientPage;

  constructor() {

  }
}