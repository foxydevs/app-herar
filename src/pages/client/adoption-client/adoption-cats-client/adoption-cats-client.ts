import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AdoptionCatsService } from '../../../../app/service/adoption-cats.service';
import { FormAdoptionCatClientPage } from '../form-adoption-cat-client/form-adoption-cat-client';

@Component({
  selector: 'adoption-cats-client',
  templateUrl: 'adoption-cats-client.html',
})
export class AdoptionCatsClientPage {
  //PROPIEDADES
  private idUser:any;
  selectItem:any = 'proceso';
  private adoptionsCats:any[] = [];
  private adoptionsCats1:any[] = [];
  private adoptionsCats2:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public modal: ModalController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: AdoptionCatsService,
  ) {
    this.idUser = localStorage.getItem('currentId')
  }

  openForm(parameter?:any) {
    this.navCtrl.push(FormAdoptionCatClientPage, { parameter });
  }

  /*openFormMotivation(parameter?:any) {
    this.selectItem = 'gatos';
    this.navCtrl.push(FormAdoptionCatClientPage, { parameter });
  }*/

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllClients(id)
    .then(response => {
      this.adoptionsCats = []
      this.adoptionsCats1 = []
      this.adoptionsCats2 = []
      this.adoptionsCats = response;
      this.adoptionsCats = response;
      this.adoptionsCats1 = response;
      this.adoptionsCats2 = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }
/*
  //CARGAR LOS RETOS
  public getAllSecond(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAllClients(id)
    .then(response => {
      this.adoptionsCats = []
      this.adoptionsCats = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }*/

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //ENVIAR FORMULARIO
  public sendEmailCat(id:string){
    this.mainService.sendEmail(id)
    .then(response => {
      console.log(response);
      this.confirmation('Revisa tu correo!', 'Te hemos enviado el formulario al correo: ');
    }).catch(error => {
      console.clear
    })
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la encuesta?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.getAll(this.idUser)
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }
/*
  //ENVIAR FORMULARIO
  public sendEmailDog(id:string){
    this.mainService.sendEmail(id)
    .then(response => {
      console.log(response);
      this.confirmation('Revisa tu correo!', 'Te hemos enviado el formulario al correo: ');
    }).catch(error => {
      console.clear
    })
  }
*/

}
