import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, NavParams} from 'ionic-angular';
import { ToastController } from '../../../../../node_modules/ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { AdoptionCatsService } from '../../../../app/service/adoption-cats.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'form-adoption-cat-client',
  templateUrl: 'form-adoption-cat-client.html',
})
export class FormAdoptionCatClientPage {
  //PROPIEDADES
  private previousNext:any;
  private disabledBtn:boolean;
  private idUser:any = path.id;
  private idClient:any = localStorage.getItem('currentId');
  private basePath:string = path.path;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  private title:any;
  private parameter:any;
  private data = {
    id: '',
    user: this.idClient,
    autoriza: this.idUser,
    nombreMascota: '',
    nombreAdoptante: '',
    edadAdoptante: '',
    dpiAdoptante: '',
    telefonoCasa: '',
    telefonoCelular: '',
    telefonoTrabajo: '',
    direccionTrabajo: '',
    direccionCasa: '',
    correo: '',
    noFamilia: '',
    noNino: '',
    edadesNino: '',
    compromiso1: 0,
    compromiso2: 0,
    nombreAdulto1: '',
    edadAdulto1: '',
    telefonoAdulto1: '',
    nombreAdulto2: '',
    edadAdulto2: '',
    telefonoAdulto2: '',
    nombreAdulto3: '',
    edadAdulto3: '',
    telefonoAdulto3: '',
    pregunta1: '',
    pregunta2: '',
    pregunta3: '',
    pregunta4: '',
    pregunta5: '',
    pregunta6: '',
    pregunta7: '',
    pregunta8: '',
    pregunta9: '',
    pregunta10: '',
    pregunta11: '',
    pregunta12: '',
    pregunta13: '',
    pregunta14: '',
    pregunta15: '',
    pregunta16: '',
    pregunta25: '',
    aprobacion: 0,
    state: 2,
    firma: 'https://es.seaicons.com/wp-content/uploads/2015/11/attach-2-icon.png',
    picture: 'https://es.seaicons.com/wp-content/uploads/2015/11/attach-2-icon.png',
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public navParams: NavParams,
    public mainService: AdoptionCatsService
  ) {
    this.previousNext = 'page1';
    this.parameter = this.navParams.get('parameter');
    if(this.parameter) {
      this.title = "Edición Adopción Gato";
      this.getSingle(this.parameter);  
    } else {
      this.title = "Agregar Adopción Gato";
    }
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      console.log(response)
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  public previousNextForm(palabra: any) {
    this.previousNext = palabra;
  }

  saveChanges() {
    if(this.parameter) {
      this.update();
    } else {
      this.create();
    }
  }

  //AGREGAR
  create() {
    this.data.firma = $('img[alt="Avatar"]').attr('src');
    this.data.picture = $('img[alt="Avatar2"]').attr('src');
    console.log(this.data)
    this.disabledBtn = true;
    this.mainService.create(this.data)
    .then(response => {
      console.log(response)
      this.data.id = response.id;
      this.navCtrl.pop();
      this.confirmation('Encuesta Envíada', 'La encuesta fue envíada, este pendiente a la respuesta.');
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update() {
    this.data.firma = $('img[alt="Avatar"]').attr('src');
    this.data.picture = $('img[alt="Avatar2"]').attr('src');
    console.log(this.data)
    this.disabledBtn = true;
    this.mainService.update(this.data)
    .then(response => {
      console.log(response)
      this.data.id = response.id;
      this.navCtrl.pop();
      this.confirmation('Encuesta Actualizada', 'La encuesta fue envíada, este pendiente a la respuesta.');
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'adopciones'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  uploadImage2(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar2').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'adopciones'
          },
          function(respuesta) {
            $('#imgAvatar2').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

}
