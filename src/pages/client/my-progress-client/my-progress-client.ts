import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { ProgressService } from '../../../app/service/progress.service';
import { AlertController } from '../../../../node_modules/ionic-angular/components/alert/alert-controller';
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'my-progress-client',
  templateUrl: 'my-progress-client.html'
})
export class MyProgressClientPage {
  //PROPIEDADES
  selectItem:any = 'inicio';
  fechaMedicion:any;
  private data:any[] = [];
  private data1:any[] = [];
  private data2:any[] = [];
  private data3:any[] = [];
  private months:any[] = [];
  idClient = localStorage.getItem('currentId');
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: ProgressService,
    public secondService: MembershipService
  ) {
    this.getMonths();
    this.getAll(this.idClient);
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(+this.membresiaClient < +this.nivelMembresia) {
      this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
    }
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    this.mainService.getAllProgress(id)
    .then(response => {
      this.data = [];
      this.data = response;
      if(this.selectItem == 'inicio') {
        this.inicio();
      } else if(this.selectItem == 'control') {
        this.intermedio();
      } else if(this.selectItem == 'final') {
        this.actual();
      }
    }).catch(error => {
      console.clear
    })
  }
  
  inicio() {
    this.loading.create({
      content: 'Cargando...',
      duration: 500
    }).present();
    if(this.data[0]) {
      this.data1 = [];
      this.data1 = this.data[0];
      this.fechaMedicion = this.returnDate(this.data[0].opcion1);
    }
  }

  intermedio() {
    this.loading.create({
      content: 'Cargando...',
      duration: 500
    }).present();
    if(this.data[this.data.length - 2]) {
      this.data2 = [];
      this.data2 = this.data[this.data.length - 2];
      this.fechaMedicion = this.returnDate(this.data[this.data.length - 2].opcion1);
    } else if(this.data[0]) {
      this.data2 = [];
      this.data2 = this.data[0];
      this.fechaMedicion = this.returnDate(this.data[0].opcion1);
    }
  }

  actual() {
    this.loading.create({
      content: 'Cargando...',
      duration: 500
    }).present();
    if(this.data[this.data.length - 1]) {
      this.data3 = [];
      this.data3 = this.data[this.data.length - 1];
      this.fechaMedicion = this.returnDate(this.data[this.data.length - 1].opcion1);
    } else if(this.data[0]) {
      this.data3 = [];
      this.data3 = this.data[0];
      this.fechaMedicion = this.returnDate(this.data[0].opcion1);
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:any) {
    var date = new Date(fechaJSON);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth())] + ', ' + date.getFullYear();
    return created_at;
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idClient);
      refresher.complete();
    }, 2000);
  }
}