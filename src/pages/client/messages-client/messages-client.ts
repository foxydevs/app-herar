import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { MessagesService } from '../../../app/service/messages.service';
import { SendMessagesClientPage } from './send-messages-client/send-messages-client';
import { UsersService } from '../../../app/service/users.service';
import { MessagesUserClientPage } from './messages-user-client/messages-user-client';
import { path } from '../../../app/config.module';
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'messages-client',
  templateUrl: 'messages-client.html'
})
export class MessagesClientPage {
  //Propiedades
  private idClient:any;
  idUser:any = path.id;
  private messages:any[] = [];
  private messagesTemp2:any[] = [];
  private users:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public messagesService: MessagesService,
    public usersService: UsersService,
    public secondService: MembershipService
  ) {
    this.idClient = localStorage.getItem("currentId");
    this.loadAllUsers();
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(+this.membresiaClient < +this.nivelMembresia) {
      this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
    }
  }

  //OBTENER MENSAJES
  public getMessages(id:any) {
    let messagesTemp:any[] = [];
    this.messagesService.getUserReceipt(id)
    .then(response => {
      for(let x of response) {
        //console.log(x.user_send)
        let user = {
          user: this.returnNameUser(x.user_send),
          picture: this.returnPicture(x.user_send),
          id: x.user_send,
          id_message: x.id
        }
        messagesTemp.push(user);
      }
      this.messagesTemp2 = this.removeDuplicates(messagesTemp, 'id');
      console.clear;
    }).catch(error => {
      console.clear;
    })
    this.messagesService.getUserSend(id)
    .then(response => {
      for(let x of response) {
        //console.log(x.user_receipt)
        let user = {
          user: this.returnNameUser(x.user_receipt),
          picture: this.returnPicture(x.user_receipt),
          id: x.user_receipt,
          id_message: x.id
        }
        messagesTemp.push(user);
      }
      this.messagesTemp2 = this.removeDuplicates(messagesTemp, 'id');    
      console.clear;
    }).catch(error => {
      console.clear;
    })
  }

  public sendMessage() {
    this.navCtrl.push(SendMessagesClientPage);
  }

  //RETORNAR EL NOMBRE DE USUARIO
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id == idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //CARGAR USUARIOS
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //RETORNAR IMAGEN DE PERFIL
  public returnPicture(idUser:any):any {
    for(var i = 0; i < this.users.length; i++) {
      if(this.users[i].id == idUser) {
        return this.users[i].picture;
      }
    }
  }

  public messagesClientUser(parameter:any) {
    this.navCtrl.push(MessagesUserClientPage, { parameter });
  }

  //REMOVER DUPLICADOS
  public removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
     return newArray;
  }

  //ORDENAR MENSAJES
  public getMessagesOrdered() {
    setTimeout(() => {
      this.getMessages(this.idClient);      
    }, 2000);
    setTimeout(() => {
      this.compare();      
      this.messages = this.messagesTemp2;
    }, 3000);
  }

  public compare() {
    this.messagesTemp2.sort(function(a, b) {
      return a.id_message-b.id_message; /* Modificar si se desea otra propiedad */
    });
    this.messagesTemp2.reverse();
    localStorage.setItem('currentMessages', JSON.stringify(this.messagesTemp2));
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.messages = JSON.parse(localStorage.getItem('currentMessages'));
    this.getMessagesOrdered();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getMessagesOrdered();
      refresher.complete();
    }, 2000);
  }

}
