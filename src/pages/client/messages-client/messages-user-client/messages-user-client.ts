import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { MessagesService } from '../../../../app/service/messages.service';
//import { SendMessagesUserClientPage } from './messages-user-send-client/messages-user-send-client';

//import { path } from "./../../../../app/config.module";
import { SendMessagesClientPage } from '../send-messages-client/send-messages-client';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@Component({
  selector: 'messages-user-client',
  templateUrl: 'messages-user-client.html'
})
export class MessagesUserClientPage {
  //PROPIEDADES
  private idClient:any;
  private messages:any[] = [];
  private message = {
    subject: 'Sin Asunto',
    message : '',
    user_send: '',
    user_receipt: '',
    picture: '',
  }
  private parameter:any;
  private id:any;
  private idUserSend:any;
  private idUserReceipt:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  //private baseId:number = path.id;

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public messagesService: MessagesService,
    private photoViewer: PhotoViewer
  ) {
    this.parameter = this.navParams.get('parameter');
    this.idClient = localStorage.getItem("currentId");
    this.id = this.parameter;
    this.loadMessages(this.idClient, this.parameter);
    this.idUserSend = localStorage.getItem('currentId');
    this.idUserReceipt = this.parameter;
    this.message.user_send = localStorage.getItem('currentId');
    this.message.user_receipt = this.parameter;
  }

  //CARGAR MENSAJES
  public loadMessages(user_send:any, user_receipt:any) {
    this.messagesService.getAll()
    .then(response => {
      this.messages = [];
      for(let x of response) {
        if(x.user_send == user_send && x.user_receipt == user_receipt
          || (x.user_send == user_receipt && x.user_receipt == user_send)) {
          this.messages.push(x);
        }
      }
      this.messages.reverse();
    }).catch(error => {
        console.clear
    })
  }

  //ENVIAR MENSAJES
  public sendMessage(parameter:any) {
    this.navCtrl.push(SendMessagesClientPage, { parameter });
  }

  ionViewWillEnter() {
    this.loadMessages(this.idClient, this.parameter);
  }

  seePicture(link:any) {
    //alert('Ver Foto')
    this.photoViewer.show(link);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadMessages(this.idClient, this.parameter);
      refresher.complete();
    }, 2000);
  }

  //AGREGAR
  sendMessages() {
    console.log(this.message)
    this.messagesService.create(this.message)
    .then(response => {
      console.log(response);
      this.message.message = '';
      this.loadMessages(this.idClient, this.parameter);
    }).catch(error => {
      console.clear
    });
  }

}
