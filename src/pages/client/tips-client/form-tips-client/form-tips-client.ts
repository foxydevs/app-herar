import { Component } from '@angular/core';
import { NavController, ViewController, LoadingController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { TipsService } from '../../../../app/service/tips.service';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Component({
  selector: 'form-tips-client',
  templateUrl: 'form-tips-client.html'
})
export class FormTipsClientPage {
  private title:any;
  private parameter:any;
  private btnDisabled:boolean = false;
  private news = {
    id: '',
    title: '',
    user: '',
    description: ''
  }

  constructor(public navCtrl: NavController,
  public loading:LoadingController,
  public navParams: NavParams,
  public toast: ToastController,
  public view: ViewController,
  public alertCtrl: AlertController,
  public newsService: TipsService,) {
    this.parameter = this.navParams.get('parameter')
    if(!this.parameter) {
      this.title = 'Ingreso de Datos'
    } else {
      this.title = 'Edición de Datos'
      this.loadNew(this.parameter);
    }
  }

  //CARGAR
  public loadNew(id:any) {
    this.newsService.getSingle(id)
    .then(res => {
      this.news.title = res.title;
      this.news.description = res.description;
      this.news.id = res.id;
      this.news.user = res.user;
    }).catch(error => {
      console.log(error)
    })
  }

  //INSERTAR DATOS
  public saveChanges() {
    this.news.user = localStorage.getItem('currentId');
    if(this.news.title) {
      if(this.news.description) {
          if(!this.parameter) {
            this.loading.create({
              content: "Cargando...",
              duration: 1000
            }).present();
            this.newsService.create(this.news)
            .then(response => {
              this.navCtrl.pop();
              this.confirmation('Tip Agregado', 'El tip fue agregado exitosamente.');
            }).catch(error => {
              this.btnDisabled = false;
              console.clear
            })
          } else {
            this.loading.create({
              content: "Cargando...",
              duration: 1000
            }).present();
            this.newsService.update(this.news)
            .then(response => {
              this.navCtrl.pop();
              this.confirmation('Tip Actualizado', 'El Tip fue actualizado exitosamente.');
            }).catch(error => {
              this.btnDisabled = false;
              console.clear
            })
          }
      } else {
        this.message('La descripción es requerida.')
      }
    } else {
      this.message('El título es requerido.')
    }
  }

  //MENSAJE DE CONFIRMACION
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

}
