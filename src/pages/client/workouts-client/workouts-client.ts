import { Component } from '@angular/core';
import { NavController, AlertController} from 'ionic-angular';
import { WorkoutsService } from '../../../app/service/workouts.service';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { DetailWorkoutsClientPage } from './detail-workouts-client/detail-workouts-client';
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'workouts-client',
  templateUrl: 'workouts-client.html',
})
export class WorkoutsClientPage {
  //PROPIEDADES
  private table:any[] = [];
  private idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: WorkoutsService,
    public secondService: MembershipService
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(+this.membresiaClient < +this.nivelMembresia) {
      this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
    }
  }

  openForm(parameter?:any) {
    this.navCtrl.push(DetailWorkoutsClientPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

}
