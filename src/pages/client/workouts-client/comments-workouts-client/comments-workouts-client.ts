import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { WorkoutsService } from '../../../../app/service/workouts.service';

@Component({
  selector: 'comments-workouts-client',
  templateUrl: 'comments-workouts-client.html'
})
export class CommentsWorkoutsClientPage{
  private comment = {
    comment: '',
    reto : '',
    user: ''
  }
  private parameter:any;
  //private baseId:number = path.id;
  private btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: WorkoutsService,
    public viewCtrl: ViewController,
    public loading: LoadingController
  ) {
    this.btnDisabled = false;
    this.parameter = this.navParams.get('parameter');
    this.comment.reto = this.parameter;
    this.comment.user = localStorage.getItem('currentId');
  }

   //Insertar Datos
   insert(){
    //let id = this.comment.product;
    if(this.comment.comment) {
      this.btnDisabled = true;
      let load =  this.loading.create({
        content: "Registrando..."
      });
      load.present();   
      this.mainService.createComment(this.comment)
      .then(response => {
          console.log(response)
        this.navCtrl.pop();
        load.dismiss();
      }).catch(error => {
          console.log(error)
          this.btnDisabled = false;
      })
    } else {
      this.toast.create({
        message: "Ingrese un comentario.",
        duration: 800
      }).present();
    }
  }
}
