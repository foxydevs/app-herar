/**
 * @author    ThemesBuckets <themebucketbd@gmail.com>
 * @copyright Copyright (c) 2018
 * @license   Fulcrumy
 * 
 * This File Represent Home Component
 * File path - '../../src/pages/home/home'
 */

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
//import { YoutubeProvider } from '../../providers/youtube/youtube';
import * as _ from 'lodash';
import { YoutubeProvider } from '../../../providers/youtube/youtube';
import { SeeYoutubeClientPage } from './see-youtube-client/see-youtube-client';
import { ListYoutubeClientPage } from './list-youtube-client/list-youtube-client';
import { AlertController } from '../../../../node_modules/ionic-angular/components/alert/alert-controller';
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'youtube-client',
  templateUrl: 'youtube-client.html',
})
export class YoutubeClientPage {
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  selectItem:any = 'videos';
  
  // List of Video Playlist
  playlists: any = [];

  // List of Videos
  videos: any = [];

  // Next Page Token
  nextPageToken: any;

  // Track Next Page Video is Exist or Not
  finished: Boolean = false;

  // List of Sorting Options
  orders = ['rating', 'date', 'relevance', 'title', 'videoCount', 'viewCount'];

  // Default Sorting Option
  orderBy: any = 'rating';

  // Track Order Change Status
  isOrderChange: Boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public youtubeProvider: YoutubeProvider,
    public secondService: MembershipService
  ) {
    this.secondService.calculateMembership();
  }

  /** Do any initialization */
  ngOnInit() {
    this.getHomeVideos(this.orderBy, '');
    this.getPlaylist('');
    //MEMBRESIA
    if(+this.membresiaClient < +this.nivelMembresia) {
      this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  /**
   * --------------------------------------------------------------
   * Get Home Page Videos
   * --------------------------------------------------------------
   * @param orderBy  Video Sorting Value
   * @param tokne     Next page token Id
   */
  async getHomeVideos(orderBy, token) {
    this.youtubeProvider.getHomeVideos(orderBy, token).subscribe(data => {
      if (this.isOrderChange) {
        this.videos = data.items;
        this.isOrderChange = false;
      } else {
        this.videos = _.uniqBy(this.videos.concat(data.items), 'id.videoId');
      }
      if (data.nextPageToken) {
        this.nextPageToken = data.nextPageToken;
      } else {
        this.finished = true;
      }
    })
  }

  /**
   * --------------------------------------------------------------
   * Get Video Playlist
   * --------------------------------------------------------------
   * @param token     Next page token Id
   */
  async getPlaylist(token) {
    this.youtubeProvider.getPlaylist(token).subscribe((data: any) => {

      this.playlists = _.uniqBy(this.playlists.concat(data.items), 'id');

      if (data.nextPageToken) {
        this.nextPageToken = data.nextPageToken;
      } else {
        this.finished = true;
      }
    })
  }

  /**
   * --------------------------------------------------------------
   * Video Details Page
   * --------------------------------------------------------------
   * @param video 
   * This method open a modal that represent specific video details page.
   */
  openVideo(video) {
    //let modal = this.modalCtrl.create('SeeYoutubeClientPage', { videoId: video.id.videoId });
    
    //modal.present();
    this.navCtrl.push(SeeYoutubeClientPage, { videoId: video.id.videoId })
  }

  /**
   * --------------------------------------------------------------
   * Open Playlist Items Page
   * --------------------------------------------------------------
   * @param id      Specific Playlist Id
   * 
   * This method open a modal page to display specific playlist items.
   */
  openPlaylistItems(id) {
    //let modal = this.modalCtrl.create('PlaylistItemsPage', { playlistId: id });
    //modal.present();
    this.navCtrl.push(ListYoutubeClientPage, { playlistId: id })
  }

  /**
   * --------------------------------------------------------------
   * Change Sorting Orders
   * --------------------------------------------------------------
   * @param orderBy 
   */
  changeOrder(orderBy) {
    this.isOrderChange = true;
    this.getHomeVideos(this.orderBy, '');
  }

  /**
   * --------------------------------------------------------------
   * Refresh Page From Top
   * --------------------------------------------------------------
   */
  doRefresh(event) {
    if(this.selectItem == 'videos') {
      this.getHomeVideos(this.orderBy, '').then(() => {
        event.complete();
      });
    } else {
      this.getPlaylist('').then(() => {
        event.complete();
      });
    }
  }

  /**
   * --------------------------------------------------------------
   * Infinite Scroll
   * --------------------------------------------------------------
   * @method doInfinite
   * 
   * The Infinite Scroll allows to perform an action when the user
   * scrolls a specified distance from the bottom or top of the page,
   * and load page next videos
   */
  doInfinite(event) {
    if(this.selectItem == 'videos') {
      setTimeout(() => {
        this.getHomeVideos(this.orderBy, this.nextPageToken).then(() => {
          event.complete();
        });
      }, 1000);
    } else {
      setTimeout(() => {
        this.getPlaylist(this.nextPageToken).then(() => {
          event.complete();
        });
      }, 1000);
    }
  }
}
