import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';

@Component({
  selector: 'my-membership-client',
  templateUrl: 'my-membership-client.html'
})
export class MyMembershipClientPage {
  //PROPIEDADES
  private profilePicture:any;
  private user:any;
  private email:any;
  private data:any[] = [];
  private months:any[] = [];
  private fechaInicio:any;
  private fechaFin:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  idUser:any = localStorage.getItem('currentId');

  constructor(
    public navCtrl: NavController,
    public mainService: UsersService,
    public loading: LoadingController
  ) {
    this.getMonths();
    this.profilePicture = localStorage.getItem('currentPicture');
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      this.fechaInicio = this.returnDate(response.inicioMembresia)
      this.fechaFin = this.returnDate(response.finMembresia)
      localStorage.setItem('currentMembresia', response.tipoNivel);
      localStorage.setItem('currentMembresiaFin', response.finMembresia)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:string) {
    var date = new Date(fechaJSON);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth())] + ', ' + date.getFullYear();
    return created_at;
  }

  ionViewWillEnter() {
    this.getSingle(this.idUser)
    this.profilePicture = localStorage.getItem("currentPicture");
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
  }

}