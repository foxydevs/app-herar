import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { path } from '../../../app/config.module';
import { BlogService } from '../../../app/service/blog.service';
import { DetailBlogClientPage } from './detail-blog-client/detail-blog-client';
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'blog-client',
  templateUrl: 'blog-client.html',
})
export class BlogClientPage {
  //PROPIEDADES
  private table:any[] = [];
  private months:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: BlogService,
    public secondService: MembershipService
  ) {
    this.secondService.calculateMembership();
  }

  openForm(parameter?:any) {
    this.navCtrl.push(DetailBlogClientPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.table = []
      for(let x of response) {
        let data = {
          picture: x.picture,
          title: x.title,
          description: x.description,
          created_at: this.returnDate(x.created_at),
          id: x.id
        }
        this.table.push(data);
      }
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:any) {
    var date = new Date(fechaJSON);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth())] + ', ' + date.getFullYear();
    return created_at;
  }

  ionViewWillEnter() {
    this.getMonths();
    this.getAll();
  }

}
