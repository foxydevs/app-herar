import { Component } from '@angular/core';
import { NavController, ViewController, LoadingController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { OrdersService } from '../../../app/service/orders.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
//import { ClientPage } from '../client';
//import { CategorysUserPage } from '../../user/categorys-user/categorys-user';
import { CategorysClientPage } from '../categorys-client/categorys-client';

//JQUERY
//declare var $:any;

@Component({
  selector: 'pagadito-client',
  templateUrl: 'pagadito-client.html'
})
export class PagaditoAdminPage {
  private parameter:any;
  //private hello:any

  constructor(public navCtrl: NavController,
  public loading:LoadingController,
  public navParams: NavParams,
  public ordersService:OrdersService,
  private iab: InAppBrowser,
  public toast: ToastController,
  public view: ViewController) {
    this.parameter = this.navParams.get('parameter')
    console.log(this.parameter)
    const browser = this.iab.create(this.parameter, '_blank', 'location=yes');
    browser.on('exit').subscribe((data) =>  {
      console.log(data)
      this.loading.create({
        content: 'Cerrando...',
        duration: 1500
      }).present();
      this.view.dismiss();
      this.navCtrl.setRoot(CategorysClientPage)
    });
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss();
  }    
  
}
