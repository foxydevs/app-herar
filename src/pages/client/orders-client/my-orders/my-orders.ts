import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { OrdersService } from '../../../../app/service/orders.service';
import { DetailOrdersClientPage } from '../detail-orders-client/detail-orders-client';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CategorysClientPage } from '../../categorys-client/categorys-client';
import { OrdersClientPage } from '../orders-client';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { MembershipService } from '../../../../app/service/membership.service';

@Component({
  selector: 'my-orders',
  templateUrl: 'my-orders.html'
})
export class MyOrdersClientPage implements OnInit {
  //PROPIEDADES
  private idClient:any;
  private parameter = {
    ern: ''
  }
  private url:any;
  private orders:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public productsService: ProductsService,
    public ordersService: OrdersService,
    public modal: ModalController,
    public iab: InAppBrowser,
    public alertCtrl: AlertController,
    public mainService: MembershipService
  ) {
    this.idClient = localStorage.getItem("currentId");
    this.mainService.calculateMembership();
    this.getAll(this.idClient);
  }

  ngOnInit() {
  }

  //CARGAR ORDENES
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.ordersService.getForClients(id)
    .then(response => {
      this.orders = []
      this.orders = response;
      this.orders.reverse();
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //VER DETALLES DE LA ORDEN
  public detailOrder(ern:any, token:any, id:any) {
    let parameter = {
      ern: ern,
      tokenID: token,
      id: id,
      type: 'detail'
    }
    let chooseModal = this.modal.create(DetailOrdersClientPage, { parameter });
    chooseModal.present();
  }

  //CANCELAR ORDEN
  public cancel(id:string){
    let order = {
      state: '0',
      id: id
    }
    let confirm = this.alertCtrl.create({
      title: '¿Deseas cancelar tu pedido?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Cancelando..."
            });
            load.present();
            this.ordersService.update(order)
            .then(response => {                 
              this.getAll(this.idClient);
              load.dismiss();
              console.clear
            }).catch(error => {
              console.clear
            });
          }
        }
      ]
    });
    confirm.present();
  }

  //ACEPTAR ORDEN
  public accept(id:string){
    let order = {
      state: '1',
      id: id
    }
    let confirm = this.alertCtrl.create({
      title: 'Su pedido a sido entregado?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Cargando..."
            });
            load.present();
            this.ordersService.update(order)
            .then(response => {                 
              this.getAll(this.idClient);
              load.dismiss();
              console.clear
            }).catch(error => {
              console.clear
            });
          }
        }
      ]
    });
    confirm.present();
  }

  //PAGAR ORDEN
  public payProduct(o:any) {
    let orden = {
      cantidad: o.quantity,
      descripcion: o.products.name,
      precio: o.unit_price,
      id: o.id,
      url: "http://me.gtechnology.gt",
      ern: o.ern,
      state: 4
    }
    this.parameter.ern = o.ern;
    this.ordersService.pagar(orden)
    .then(res => {
      this.loading.create({
        content: "Realizando Pedido...",
        duration: 2000
      }).present();
      let url = res.token;
      let parameter = {
        ern: this.parameter.ern,
        tokenID: ''
      }
      //CARGAR ULTIMO LINK
      const browser = this.iab.create(url, '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');
      browser.on('loadstart').subscribe((e) => {
        this.url = e.url;
        //URL TOKEN NUEVOhttp://toktok.foxylabs.xyz/home/comprobante/ca4d0a9023b11b5600d1598868ed1827/02t778
        let tokenUrlComparation = e.url;
        //URL TOKEN A COMPARAR
        let token = this.url.replace('http://toktok.foxylabs.xyz/home/comprobante/','');
        let ernReplace = '/' + orden.ern;
        let tokenFinally = token.replace(ernReplace, '')
        parameter.tokenID = tokenFinally;
        let urlFinally = 'http://toktok.foxylabs.xyz/home/comprobante/' + tokenFinally + ernReplace
        if(tokenUrlComparation == urlFinally) {
          //alert('CONGRATULATIONS')
          if(tokenFinally.length >= 30) {
            this.navCtrl.setRoot(MyOrdersClientPage)
            browser.close()
            this.loading.create({
              content: 'Procesando Compra...',
              duration: 3000
            }).present();
            let chooseModal = this.modal.create(DetailOrdersClientPage, { parameter });
            chooseModal.present();
          } else {
            this.loading.create({
              content: 'Saliendo de la Compra...',
              duration: 2000
            }).present();
            this.navCtrl.setRoot(CategorysClientPage)
          }  
        }
      });
      //CARGAR SALIDA
      browser.on('exit').subscribe((data) =>  {
        this.loading.create({
          content: 'Saliendo de la Compra...',
          duration: 2000
        }).present();
        this.navCtrl.setRoot(OrdersClientPage)
      });
    }).catch(error => {
      console.log(error)
    })
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idClient);
      refresher.complete();
    }, 2000);
  }

}
