import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChangePasswordProfileClientPage } from './change-password-client/change-password-client';
import { ProfileUpdateClientPage } from './profile-update-client/profile-update-client';
import { UsersService } from '../../../app/service/users.service';

@Component({
  selector: 'profile-client',
  templateUrl: 'profile-client.html'
})
export class ProfileClientPage {
  //Propiedades
  private profilePicture:any;
  private user:any;
  private email:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  constructor(
    public navCtrl: NavController,
    public usersService: UsersService
  ) {
    this.profilePicture = localStorage.getItem('currentPicture');
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
  }

  //Cambiar Contraseña
  public changePassword() {
    this.navCtrl.push(ChangePasswordProfileClientPage);
  }

  //Actualizar Usuario
  public updateClient() {
    this.navCtrl.push(ProfileUpdateClientPage);
  }

  ionViewWillEnter() {
    this.profilePicture = localStorage.getItem("currentPicture");
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
  }

}