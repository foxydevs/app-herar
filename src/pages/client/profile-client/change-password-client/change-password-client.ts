import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';
import { ProfileClientPage } from '../profile-client'

@Component({
  selector: 'change-password-client',
  templateUrl: 'change-password-client.html'
})
export class ChangePasswordProfileClientPage {
  //Propiedades
  private changePassword = {
    old_pass: '',
	new_pass : '',
	new_pass_rep: '',
    id: ''
  }
  private idClient:any;
  private btnDisabled:boolean = false;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  constructor(
    public navCtrl: NavController,
    public clientsService: UsersService,
    public toast: ToastController,
    public loading: LoadingController
  ) {
      this.idClient
  }

  //GUARDAR CAMBIOS
  public updatePassword() {
    if(this.changePassword.old_pass) {
      if(this.changePassword.new_pass) {
        if(this.changePassword.new_pass_rep) {
          if(this.changePassword.old_pass != this.changePassword.new_pass) {
            if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
              if(this.changePassword.new_pass.length >= 8) {
                this.btnDisabled = true;
                this.clientsService.changePassword(this.changePassword)
                .then(res => {
                  this.loading.create({
                    content: "Cambiando Contraseña...",
                    duration: 500
                  }).present();
                  this.navCtrl.setRoot(ProfileClientPage)
                }).catch(error => {
                  this.message('La contraseña no es válida.')
                  this.btnDisabled = false;
                });
              } else {
                this.message('La contraseña debe tener al menos 8 caracteres.')
              }
            } else {
              this.message('Las contraseñas no son iguales.')
            }
          } else {
            this.message('No puede usar la misma contraseña.')
          }
        } else {
          this.message('Confirmar contraseña es requerido.')
        }
      } else {
        this.message('La contraseña nueva es requerida.')
      }
    } else {
      this.message('La contraseña actual es requerida.')
    }
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 500
    }).present();
  }

}
