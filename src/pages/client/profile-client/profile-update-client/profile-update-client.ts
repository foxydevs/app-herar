import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProfileClientPage } from '../profile-client'
import { path } from "../../../../app/config.module";
import { UsersService } from '../../../../app/service/users.service';
import { Events } from 'ionic-angular/util/events';

//JQUERY
declare var $:any;

@Component({
  selector: 'profile-update-client',
  templateUrl: 'profile-update-client.html'
})
export class ProfileUpdateClientPage {
  //Propiedades
  private idClient:any;
  private basePath:string = path.path;
  private btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  private profile = {
    picture: '',
  }
  private profileUpdate = {
      username: '',
      email: '',
      firstname: '',
      lastname: '',
      work: '',
      description: '',
      age: '',
      birthday: '',
      phone: '',
      id: 0
  }
  
  //Constructor
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public userService: UsersService,
    public events: Events,
    public loading: LoadingController
  ) {
    this.idClient = localStorage.getItem('currentId');
    this.userService.getSingle(this.idClient)
    .then(response => {
        this.profile.picture = response.picture;
        this.profileUpdate.username = response.username;
        this.profileUpdate.email = response.email;
        this.profileUpdate.firstname = response.firstname;
        this.profileUpdate.lastname = response.lastname;
        this.profileUpdate.work = response.work;
        this.profileUpdate.description = response.description;
        this.profileUpdate.age = response.age;
        this.profileUpdate.birthday = response.birthday;
        this.profileUpdate.phone = response.phone;
        this.profileUpdate.id = this.idClient;
    }).catch(error => {
        console.clear();
    })
  }

  //Subir Imagenes de Perfil
  uploadImage(archivo, id) {
    let events = this.events;
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/${this.idClient}`;
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(2*(1024*1024))) {
            $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {                    
                    $('#imgAvatar').attr("src",respuesta.picture)
                    localStorage.setItem('currentPicture', respuesta.picture);
                    events.publish('user:update');
                    $("#"+id).val('')
                }
            );
            
        } else {
                this.toast.create({
                    message: "La imagen es demasiado grande.",
                    duration: 1500
                }).present();
        }
    } else {
        this.toast.create({
            message: "El tipo de imagen no es válido.",
            duration: 1500
        }).present();
    }
  }

  //Insertar Datos
  public update(){
    this.btnDisabled = true;
    let events = this.events;
    this.userService.update(this.profileUpdate)
    .then(response => {      
      let loader = this.loading.create({
        content: "Actualizando Cuenta...",
        duration: 2000
      });
      localStorage.setItem('currentFirstName', this.profileUpdate.firstname);
      localStorage.setItem('currentLastName', this.profileUpdate.lastname);
      localStorage.setItem('currentEmail', this.profileUpdate.email);
      events.publish('user:update');
      loader.present();
      this.navCtrl.setRoot(ProfileClientPage);
      console.clear
    }).catch(error => {
      this.btnDisabled = false;        
      console.clear();
    })
  }

}
