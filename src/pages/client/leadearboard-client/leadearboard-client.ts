import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController} from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { ChallengeService } from '../../../app/service/challenge.service';
import { MotivationService } from '../../../app/service/motivation.service';
import { DetailChallengeUserPage } from './detail-challenges-client/detail-challenges-client';
import { DetailMotivationUserPage } from './detail-motivations-client/detail-motivations-client';
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'leadearboard-client',
  templateUrl: 'leadearboard-client.html',
})
export class LeadearboardClientPage {
  //PROPIEDADES
  private selectItem:any = 'retos';
  private challenges:any[] = [];
  private motivations:any[] = [];
  private members:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public modal: ModalController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: ChallengeService,
    public secondService: MotivationService,
    public thirdService: MotivationService,
    public fourthService: MembershipService
  ) {
    this.fourthService.calculateMembership();    
  }

  openFormChallenge(parameter?:any) {
    this.selectItem = 'retos';
    this.navCtrl.push(DetailChallengeUserPage, { parameter });
  }

  openFormMotivation(parameter?:any) {
    this.selectItem = 'motivacion';
    this.navCtrl.push(DetailMotivationUserPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    if(+this.membresiaClient >= +this.nivelMembresia) {
      let load = this.loading.create({
        content: 'Cargando...'
      });
      load.present();
      this.mainService.getAll()
      .then(response => {
        this.challenges = []
        this.challenges = response;
        load.dismiss();
      }).catch(error => {
        console.clear
      })
    }
  }

  //CARGAR LOS RETOS
  public getAllSecond() {
    if(+this.membresiaClient >= +this.nivelMembresia) {
      let load = this.loading.create({
        content: 'Cargando...'
      });
      load.present();
      this.secondService.getAll()
      .then(response => {
        this.motivations = []
        this.motivations = response;
        load.dismiss();
      }).catch(error => {
        console.clear
      })
    }
  }

  //GET SCORE
  getAllThird() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllScore()
    .then(response => {
      this.members = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    //MEMBRESIA
    if(+this.membresiaClient < +this.nivelMembresia) {
      this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
    }
    if(this.selectItem == 'retos') {
      this.getAll();
    } else if(this.selectItem == 'motivacion') {
      this.getAllSecond();
    }
  }

}
