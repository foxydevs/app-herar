import { Component } from '@angular/core';
import { NavController, AlertController} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PromotionService } from '../../../app/service/promotion.service';
import { DetailPromotionClientPage } from './detail-promotion-client/detail-promotion-client';
import { MembershipService } from '../../../app/service/membership.service';

@Component({
  selector: 'promotion-client',
  templateUrl: 'promotion-client.html',
})
export class PromotionClientPage {
  //PROPIEDADES
  private table:any[] = [];
  private idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');
  selectItem:any = 'beneficios';
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: PromotionService,
    public secondService: MembershipService
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.secondService.calculateMembership();    
    //MEMBRESIA
    if(+this.membresiaClient < +this.nivelMembresia) {
      this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
    }
  }

  openForm(parameter?:any) {
    this.navCtrl.push(DetailPromotionClientPage, { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

}
