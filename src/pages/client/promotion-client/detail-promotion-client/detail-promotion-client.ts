import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { PromotionService } from '../../../../app/service/promotion.service';

@Component({
  selector: 'detail-promotion-client',
  templateUrl: 'detail-promotion-client.html',
})
export class DetailPromotionClientPage {
  //PROPIEDADES
  private parameter:any;
  private data:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: PromotionService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getSingle(this.parameter);
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getSingle(this.parameter);
      refresher.complete();
    }, 2000);
  }

}
