import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { path } from "../../../app/config.module";
import { UsersService } from '../../../app/service/users.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'personalization-client',
  templateUrl: 'personalization-client.html'
})
export class PersonalizationClientPage {
  //PROPIEDADES
  private basePath:string = path.path;
  private idClient:any;
  private profilePicture1:any;
  private profilePicture2:any;
  private profilePicture3:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public usersService: UsersService
  ) {
    this.idClient = localStorage.getItem('currentId');
    this.loadPictures(this.idClient)
  }

  //LOAD PICTURES
  public loadPictures(id:any) {
    this.usersService.getSingle(id)
    .then(response => {
    this.profilePicture1 = response.pic1;
    this.profilePicture2 = response.pic2;
    this.profilePicture3 = response.pic3;
    }).catch(error => {
    })
  }

  //Subir Imagenes de Perfil
  uploadImage1(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic1/${this.idClient}`;
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(2*(1024*1024))) {
            this.loading.create({
                content: "Cargando Imagen",
                duration: 5000
            }).present();
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                    $('#imgAvatar1').attr("src",'')
                    $('#imgAvatar1').attr("src",respuesta.pic1)
                    $("#"+id).val('')
                }
            );
            
        } else {
                this.toast.create({
                    message: "La imagen es demasiado grande.",
                    duration: 1500
                }).present();
        }
    } else {
        this.toast.create({
            message: "El tipo de imagen no es válido.",
            duration: 1500
        }).present();
    }
  }

  //Subir Imagenes de Perfil
  uploadImage2(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic2/${this.idClient}`;
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(2*(1024*1024))) {
            this.loading.create({
                content: "Cargando Imagen",
                duration: 5000
            }).present();
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                    $('#imgAvatar2').attr("src",'')
                    $('#imgAvatar2').attr("src",respuesta.pic2)
                    $("#"+id).val('')
                }
            );
            
        } else {
                this.toast.create({
                    message: "La imagen es demasiado grande.",
                    duration: 1500
                }).present();
        }
    } else {
        this.toast.create({
            message: "El tipo de imagen no es válido.",
            duration: 1500
        }).present();
    }
  }

  //Subir Imagenes de Perfil
  uploadImage3(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic3/${this.idClient}`;
    console.log(archivos)
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(2*(1024*1024))) {
            this.loading.create({
                content: "Cargando Imagen",
                duration: 5000
            }).present();
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                    $('#imgAvatar3').attr("src",'')
                    $('#imgAvatar3').attr("src",respuesta.pic3)
                    $("#"+id).val('')
                }
            );
            
        } else {
                this.toast.create({
                    message: "La imagen es demasiado grande.",
                    duration: 1500
                }).present();
        }
    } else {
        this.toast.create({
            message: "El tipo de imagen no es válido.",
            duration: 1500
        }).present();
    }
  }

}