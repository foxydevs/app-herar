import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { InAppBrowser } from '../../../../node_modules/@ionic-native/in-app-browser';

@Component({
  selector: 'terms-and-conditions',
  templateUrl: 'terms-and-conditions.html'
})
export class TermsAndConditionsPage {
  //PROPIEDADES
  pictureLogin = localStorage.getItem('currentPictureLogin');
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentButtonColor');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public view: ViewController,
    public iab: InAppBrowser,) {
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //ACEPTAR TERMINOS Y CONDICIONES
  accept() {
    this.view.dismiss('Accept');    
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
  }
}
