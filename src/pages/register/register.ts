import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

//IMPORTAR SERVICIOS
import { AuthService } from '../../app/service/auth.service';
import { UsersService } from '../../app/service/users.service';
import { LoginPage } from '../login/login';

import { path } from "./../../app/config.module";
import { TermsAndConditionsPage } from './terms-and-conditions/terms-and-conditions';
import { ModalController } from '../../../node_modules/ionic-angular/components/modal/modal-controller';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  //PROPIEDADES
  private users:any[] = [];
  private btnDisabled:boolean;
  private baseId:number = path.id;
  //private baseUser:any;
  private pictureLogin:string;
  private user:any = {
    username: '',
    email: '',
    password: '',
    password_repeat: '',
    usuario: this.baseId,
    accept: false,
    firstname: '',
    lastname: '',
    work: '',
    description: '',
    age: '',
    birthday: '',
    phone: ''
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public modal: ModalController,
    public authentication: AuthService,
    public usersService: UsersService) {
    this.pictureLogin = localStorage.getItem('currentPictureLogin')
    this.loadAllUsers();
  }

  //Cargar los productos
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear();
    })
  }

  //Crear Cuenta
  public createAccount(){
    if(this.user.username || this.user.email || this.user.password
    || this.user.password_repeat || this.user.usuario || this.user.accept
    || this.user.firstname || this.user.lastname || this.user.work || this.user.description
    || this.user.age || this.user.birthday || this.user.phone) {
      if(this.user.username) {
        if(this.user.email) {
          if(this.user.password || this.user.password_repeat) {
            if(this.user.usuario) {
              if(this.user.accept == true) {
                if(this.user.password == this.user.password_repeat) {
                  if(this.user.firstname) {
                    if(this.user.lastname) {
                      if(this.user.work) {
                        if(this.user.description) {
                          if(this.user.age) {
                            if(this.user.birthday) {
                              if(this.user.phone) {
                                this.btnDisabled = true;
                                this.usersService.create(this.user)
                                .then(response => {
                                  this.loading.create({
                                    content: "Registrando...",
                                    duration: 2000
                                  }).present();
                                  console.log(this.user)
                                  this.navCtrl.setRoot(LoginPage);
                                  console.clear
                                }).catch(error => {
                                  this.btnDisabled = false;
                                  console.clear;
                                  console.log(error)
                                  this.toast.create({
                                      message: "El usuario ya existe.",
                                      duration: 800
                                    }).present();
                                })
                              } else {
                                this.toast.create({
                                  message: "El No. de teléfono es requerido.",
                                  duration: 800
                                }).present();
                              }
                            } else {
                              this.toast.create({
                                message: "El cumpleaños es requerido.",
                                duration: 800
                              }).present();
                            }
                          } else {
                            this.toast.create({
                              message: "La edad es requerida..",
                              duration: 800
                            }).present();
                          }
                        } else {
                          this.toast.create({
                            message: "Agrega una breve descripción tuya.",
                            duration: 800
                          }).present();
                        }
                      } else {
                        this.toast.create({
                          message: "El trabajo es requerido.",
                          duration: 800
                        }).present();
                      }
                    } else {
                      this.toast.create({
                        message: "El apellido es requerido.",
                        duration: 800
                      }).present();
                    }
                  } else {
                    this.toast.create({
                      message: "El nombre es requerido.",
                      duration: 800
                    }).present();
                  }
                } else {
                  this.toast.create({
                    message: "La contraseña deben ser iguales.",
                    duration: 800
                  }).present();
                }
              } else {
                this.toast.create({
                    message: "No puedes crear tu usuario si no aceptas los términos y condiciones.",
                    duration: 800
                }).present();
              }
            } else {
              this.toast.create({
                message: "Seleccione un proveedor.",
                duration: 800
              }).present();
            }
          } else {
            this.toast.create({
              message: "La contraseña es requerida.",
              duration: 800
            }).present();
          }
        } else {
          this.toast.create({
            message: "El correo es requerido.",
            duration: 800
          }).present();
        }
      } else {
        this.toast.create({
          message: "El nombre de usuario es requerido.",
          duration: 800
        }).present();
      }
    } else {
      this.toast.create({
        message: "No dejar ningun campo vacío.",
        duration: 800
      }).present();
    }
  }

  openModal() {
    let chooseModal = this.modal.create(TermsAndConditionsPage);
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        if(data == 'Accept') {
          this.user.accept = true;
        }
      }      
    });
    chooseModal.present();
  }

}
