import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

//IMPORTAR SERVICIOS
import { UsersService } from '../../app/service/users.service';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LoginPage } from '../login/login';

@Component({
  selector: 'reset-password',
  templateUrl: 'reset-password.html'
})
export class ResetPasswordPage {
  //PROPIEDADES
  private btnDisabled:boolean;
  private user:any = {
    username:""
  }
  picture:any;
  pictureLogin:string;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public toast: ToastController,
    public alertCtrl: AlertController,
    public userService: UsersService ) {
    this.picture = localStorage.getItem('currentPicture11');
    this.pictureLogin = localStorage.getItem('currentPictureLogin');
    this.btnDisabled = false;
  }


  //RECOVERY
  public resetPassword() {
    if(this.user.username) {
        this.btnDisabled = true;
        this.userService.resetPassword(this.user)
        .then(response => {
            console.log(response);
            let confirm = this.alertCtrl.create({
                title: 'Contraseña Recuperada',
                message: 'Te hemos enviado un correo a: ' + response.email,
                buttons: [{
                text: 'Ok',
                handler: () => {
                    this.navCtrl.setRoot(LoginPage);
                }
                }]
            });
            confirm.present();
        }).catch(error => {
            this.message('Usuario no encontrado.', 1000)
            this.btnDisabled = false;
            console.log(error)
        })
    } else {
      this.message('El correo es requerido.', 500)
    }
  }

  //MENSAJES
  public message(messages: any, duration: any) {
    this.toast.create({
      message: messages,
      duration: duration
    }).present();
  }

}

